﻿package  
{
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class CTimer 
	{
		private var m_repeatTime:uint;
		private var m_timeBetweenRepeatSec:Number;
		private var m_currentTime:Number;
		private var m_currentRepeatTime:uint;
		private var m_callbackFunction:Function;
		
		public function CTimer(repeatTime:uint, timeBetweenRepeatSec:Number, callbackFunction:Function) 
		{
			m_repeatTime = repeatTime;
			m_timeBetweenRepeatSec = timeBetweenRepeatSec;
			
			m_currentTime = 0;
			m_currentRepeatTime = 0;
			m_callbackFunction = callbackFunction;
			
			GameStage.instance.timers.push(this);
		}
		
		public function set timeBetweenRepeatSec(newValue:Number):void
		{
			m_timeBetweenRepeatSec = newValue;
			m_currentTime = 0;
		}
		
		public function update(dt:Number):void
		{
			m_currentTime += dt;
			
			if (m_currentTime >= m_timeBetweenRepeatSec)
			{
				m_currentTime = 0;
				
				var timers:Array = null;
				
				if (m_repeatTime == 0)
				{
					m_callbackFunction();
					return;
				}
				else
				{
					m_callbackFunction();
					m_currentRepeatTime++;
				}
				
				if (m_currentRepeatTime >= m_repeatTime)
				{
					timers = GameStage.instance.timers;
					timers.splice(timers.indexOf(this), 1);
				}
			}
		}
		
		public function deleteTimer():void
		{
			var timers:Array = GameStage.instance.timers;
			timers.splice(timers.indexOf(this), 1);
		}
	}
}