﻿package  
{
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class Resources 
	{
		[Embed(source="/res/itemsNamesToShow.xml", mimeType = "application/octet-stream")]
		public static const ItemsNamesToShowXml:Class;
		
		[Embed(source="/res/itemsCanFall.xml", mimeType = "application/octet-stream")]
		public static const ItemsCanFallXml:Class;
	
		[Embed(source="/res/subItemsInfo.xml", mimeType = "application/octet-stream")]
		public static const SubItemsInfoXml:Class;
		
		[Embed(source="/res/superItemsInfo.xml", mimeType = "application/octet-stream")]
		public static const SuperItemsInfoXml:Class;
		
		[Embed(source="/res/sounds/applause.mp3")]
		public static const ApplauseSound:Class;
		
		[Embed(source="/res/sounds/button.mp3")]
		public static const ButtonSound:Class;
		
		[Embed(source="/res/sounds/coin.mp3")]
		public static const CoinSound:Class;
		
		[Embed(source="/res/sounds/shovel.mp3")]
		public static const ShovelSound:Class;
		
		[Embed(source="/res/gnomeDigger.png")]
		public static const GnomeDiggerImage:Class;
		
		[Embed(source="/res/shells.png")]
		public static const ShellsImage:Class;
		
		[Embed(source="/res/auger.png")]
		public static const AugerImage:Class;
		
		[Embed(source="/res/button.png")]
		public static const ButtonImage:Class;
		
		[Embed(source="/res/itemsLabel.png")]
		public static const ItemsLabelImage:Class;
		
		[Embed(source="/res/stonesBackground.png")]
		public static const StonesBackgroundImage:Class;
		
		[Embed(source="/res/rakyshkaMenu.png")]
		public static const ShellMenuImage:Class;
		
		[Embed(source="/res/elephantMenu.png")]
		public static const ElephantMenuImage:Class;
		
		[Embed(source="/res/gameTitle.png")]
		public static const GameTitleImage:Class;
		
		[Embed(source="/res/particleEarth.png")]
		public static const EarthParticleImage:Class;
		
		[Embed(source="/res/grassParticle.png")]
		public static const GrassParticleImage:Class;

		[Embed(source="/res/crashImage.png")]
		public static const CrashImage:Class;
		
		[Embed(source="/res/crashEarthImage.png")]
		public static const CrashEarthImage:Class;

		[Embed(source="/res/shell2.png")]
		public static const Shell2Image:Class;

		[Embed(source="/res/shell.png")]
		public static const ShellImage:Class;

		[Embed(source="/res/oneRing.png")]
		public static const OneRingImage:Class;

		[Embed(source="/res/adamRib.png")]
		public static const AdamRibImage:Class;

		[Embed(source="/res/bone.png")]
		public static const BoneImage:Class;

		[Embed(source="/res/skilet.png")]
		public static const SkeletImage:Class;

		[Embed(source="/res/skull.png")]
		public static const SkullImage:Class;

		[Embed(source="/res/vaza.png")]
		public static const VazaImage:Class;

		[Embed(source="/res/popup.png")]
		public static const PickaxeInfoPopupImage:Class;

		[Embed(source="/res/pickaxeUpdateButton.png")]
		public static const PickaxeUpdateButtonImage:Class;

		[Embed(source="/res/pickaxe.png")]
		public static const PickaxeImage:Class;

		[Embed(source="/res/coin.png")]
		public static const CoinImage:Class;

		[Embed(source="/res/arrowButton.png")]
		public static const ArrowButtonImage:Class;

		[Embed(source="/res/pauseButton.png")]
		public static const PauseButtonImage:Class;
		
		[Embed(source="/res/closeButton.png")]
		public static const CloseButtonImage:Class;
		
		[Embed(source="/res/pauseMenuBase.png")]
		public static const PauseMenuBaseImage:Class;
		
		[Embed(source="/res/line.png")]
		public static const LineImage:Class;
		
		[Embed(source="/res/itemsMenuBase.png")]
		public static const ItemsMenuBaseImage:Class;
		
		[Embed(source="/res/buttonBaseSmall.png")]
		public static const ButtonBaseSmallImage:Class;
		
		[Embed(source="/res/buttonBase.png")]
		public static const ButtonBaseImage:Class;
		
		[Embed(source="/res/tileChest.png")]
		public static const TileChestImage:Class;
		
		[Embed(source="/res/tileEarth.png")]
		public static const TileEarthImage:Class;
		
		[Embed(source="/res/tileGrass.png")]
		public static const TileGrassImage:Class;
		
		[Embed(source="/res/Remember.ttf", fontName="RememberFont", mimeType = "application/x-font", advancedAntiAliasing="true", embedAsCFF="false")]
		public static const RememberFont:Class;
	}
}