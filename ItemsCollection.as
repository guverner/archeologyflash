﻿package 
{	
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.net.SharedObject;
	import flash.utils.Dictionary;
	import flash.geom.ColorTransform;
	import flash.events.MouseEvent;
	import flash.display.Stage;
	import flash.filters.DropShadowFilter;
	import flash.filters.GlowFilter;

	public class ItemsCollection
	{
		public static const ITEM_BONE:String = 	"Bone";
		public static const ITEM_SKULL:String = "Skull";
		public static const ITEM_VASE:String = 	"Vase";
		public static const ITEM_SKELETON:String = "Skeleton";
		public static const ITEM_SHELL:String = "Shell";
		public static const ITEM_SHELL_2:String = "Shell2";
		public static const ITEM_BUTTON:String = "Button";
		public static const ITEM_ONE_RING:String = "OneRing";
		public static const ITEM_ADAM_RIB:String = "AdamRib";
		public static const ITEM_AUGER:String = "Auger";
		public static const ITEM_SHELLS:String = "Shells";
		
		private static var 	m_instance:ItemsCollection;
		private static var 	m_isAllowInstantiation:Boolean;
		
		private var m_haveItems:Array;//for output in items menu
		private var m_dontHaveItems:Array;//for drop items from the chest
		private var m_itemsSprites:Dictionary;
		
		private static var m_allItemsCanFall:Array;
		private static var m_itemsNamesToShow:Dictionary;
		private static var m_superItemsInfo:Dictionary;//to know subitems of the superitem
		private static var m_subItemsInfo:Dictionary;//to know superitem of the subitem
		
		public function ItemsCollection()
		{
			if (!m_isAllowInstantiation)
			{
				throw new Error("Error: Instantiation failed: Use ItemsCollection.instance instead of new.");
			}
			
			initialise();
		}
		
		public static function get instance():ItemsCollection 
		{
			if (!m_instance)
			{
				m_isAllowInstantiation = true;
				m_instance = new ItemsCollection();
				m_isAllowInstantiation = false;
			}
			
			return m_instance;
		}
		
		private function initialise():void
		{
			m_haveItems = new Array();
			m_dontHaveItems = new Array();
			m_itemsSprites = new Dictionary();
			
			m_allItemsCanFall = new Array();
			m_superItemsInfo = new Dictionary();
			m_subItemsInfo = new Dictionary();
			m_itemsNamesToShow = new Dictionary();
			
			ItemsCollection.loadSuperItemsInfo();
			ItemsCollection.loadSubItemsInfo();
			ItemsCollection.loadAllItemsCanFall();
			ItemsCollection.loadItemsNamesToShow();
			
			loadHaveItems();
			createDontHaveItems();
		}
		
		private static function loadItemsNamesToShow():void
		{
			var itemsNamesToShowXml:XML = new XML(new Resources.ItemsNamesToShowXml());
			
			var item:XML = null;
			
			for each(item in itemsNamesToShowXml.item)
			{
				var itemName:String = item.@name;
				var itemNameToShow:String = item.@nameShow;
				
				m_itemsNamesToShow[itemName] = {nameShow:itemNameToShow};
			}
		}
		
		private static function loadAllItemsCanFall():void
		{
			var itemsCanFallXml:XML = new XML(new Resources.ItemsCanFallXml());
			
			var item:XML = null;
			
			for each(item in itemsCanFallXml.item)
			{
				var itemName:String = item.@name;
				m_allItemsCanFall.push(itemName);
			}
		}
		
		public static function get subItemsInfo():Dictionary
		{
			return m_subItemsInfo;
		}
		
		public static function get superItemsInfo():Dictionary
		{
			return m_superItemsInfo;
		}
		
		public static function get itemsNamesToShow():Dictionary
		{
			return m_itemsNamesToShow;
		}
		
		private static function loadSubItemsInfo():void
		{
			var subItemsInfoXml:XML = new XML(new Resources.SubItemsInfoXml());
			
			var subItem:XML = null;
			
			for each (subItem in subItemsInfoXml.item)
			{
				var subItemName:String = subItem.@name;
				var superItemName:String = subItem.@superitem;
				var subItemPrice:uint = subItem.@price;
				
				m_subItemsInfo[subItemName] = {superitem:superItemName, price:subItemPrice};
			}
		}
		
		private static function loadSuperItemsInfo():void
		{
			var superItemsInfoXml:XML = new XML(new Resources.SuperItemsInfoXml());
			
			var superItem:XML = null;
			var subItem:XML = null;
			
			for each (superItem in superItemsInfoXml.item)
			{
				var superItemName:String = superItem.@name;
				m_superItemsInfo[superItemName] = new Array();
				
				for each (subItem in superItem.subitems.item)
				{
					var subItemName:String = subItem.@name;
					(m_superItemsInfo[superItemName] as Array).push(subItemName);
				}
			}
		}
		
		private function createDontHaveItems():void
		{
			var haveItemsNames:Array = this.getHaveItemsNames();
			
			var indItemName:int = 0;
			
			if(!haveItemsNames || haveItemsNames.length == 0)
			{
				for(indItemName = 0; indItemName < m_allItemsCanFall.length; indItemName++)
				{
					m_dontHaveItems.push(createItemForName(m_allItemsCanFall[indItemName]));
				}
			}
			else
			{
				for(indItemName = 0; indItemName < m_allItemsCanFall.length; indItemName++)
				{
					var isHaveItem:Boolean = false;
					
					for(var indHaveItemName:int = 0; indHaveItemName < haveItemsNames.length; indHaveItemName++)
					{
						if(m_allItemsCanFall[indItemName] == haveItemsNames[indHaveItemName])
						{
							isHaveItem = true;
							break;
						}
					}
					
					if(!isHaveItem)
					{
						m_dontHaveItems.push(createItemForName(m_allItemsCanFall[indItemName]));
					}
				}
			}
		}
		
		private function loadDontHaveItems():Boolean
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			
			if(sharedObject.data.dontHaveItems)
			{
				var dontHaveItemsNames:Array = sharedObject.data.dontHaveItems as Array;
				
				if(dontHaveItemsNames.length == 0)
				{
					return false;
				}
				else
				{
					for(var indDontHaveItemName:int = 0; indDontHaveItemName < dontHaveItemsNames.length; indDontHaveItemName++)
					{
						m_dontHaveItems.push(createItemForName(dontHaveItemsNames[indDontHaveItemName]));
					}
					
					return true;
				}
			}
			else
			{
				return false;
			}
		}
		
		private function loadHaveItems():Boolean
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			
			if(sharedObject.data.haveItems)
			{
				var haveItemsNames:Array = sharedObject.data.haveItems as Array;
				
				if(haveItemsNames.length == 0)
				{
					return false;
				}
				else
				{
					for(var indHaveItemName:int = 0; indHaveItemName < haveItemsNames.length; indHaveItemName++)
					{
						var item:Item = createItemForName(haveItemsNames[indHaveItemName]);
						m_haveItems.push(item);
						
						var itemContainer:Sprite = initSpriteContainerForItem(item);
						
						m_itemsSprites[item.name] = itemContainer;
					}
					
					return true;
				}
			}
			else
			{
				return false;
			}
		}
		
		public function get dontHaveItems():Array
		{
			return m_dontHaveItems;
		}
		
		public function get haveItems():Array
		{
			return m_haveItems;
		}
		
		public function bitmapForItemName(itemName:String):Bitmap
		{
			switch(itemName)
			{
				case ITEM_BONE:
					return (new Resources.BoneImage() as Bitmap);
				case ITEM_SKULL:
					return (new Resources.SkullImage() as Bitmap);
				case ITEM_VASE:
					return (new Resources.VazaImage() as Bitmap);
				case ITEM_SKELETON:
					return (new Resources.SkeletImage() as Bitmap);
				case ITEM_ADAM_RIB:
					return (new Resources.AdamRibImage() as Bitmap);
				case ITEM_ONE_RING:
					return (new Resources.OneRingImage() as Bitmap);
				case ITEM_SHELL:
					return (new Resources.ShellImage() as Bitmap);
				case ITEM_SHELL_2:
					return (new Resources.Shell2Image() as Bitmap);
				case ITEM_BUTTON:
					return (new Resources.ButtonImage() as Bitmap);
				case ITEM_AUGER:
					return (new Resources.AugerImage() as Bitmap);
				case ITEM_SHELLS:
					return (new Resources.ShellsImage() as Bitmap);
			}
			
			return null;
		}
		
		public function createItemForName(itemName:String):Item
		{
			var item:Item = new Item(itemName);
			return item;
		}
		
		public function addHaveItem(item:Item):void
		{
			if(!item)
			{
				return;
			}
			
			m_dontHaveItems.splice(m_dontHaveItems.indexOf(item), 1);
			
			var itemContainer:Sprite = null;
			
			if(item.superItem)//item is subitem
			{
				GameStage.instance.removeBlackBitmap(item.name);//for not check collision
				
				var superItem:Item = this.getHaveItem(item.superItem);
				
				if(superItem == null)
				{
					superItem = createItemForName(item.superItem);
					m_haveItems.push(superItem);
				}
				else if(m_itemsSprites[superItem.name] != null)
				{
					m_itemsSprites[superItem.name] = null;
				}
				
				//add item to subitems of the its superitem
				for(var indSubItemName:int = 0; indSubItemName < superItem.subItems.length; indSubItemName++)
				{
					if(superItem.subItems[indSubItemName] is String &&
					   superItem.subItems[indSubItemName] == item.name)
					{
						superItem.subItems[indSubItemName] = item;
					}
				} 
				
				itemContainer = initSpriteContainerForItem(superItem);
				
				m_itemsSprites[superItem.name] = itemContainer;
			}
			else
			{
				m_haveItems.push(item);
				
				itemContainer = initSpriteContainerForItem(item);
				m_itemsSprites[item.name] = itemContainer;
				
				saveHaveItem(item.name);
			}
		}
		
		public function getDontHaveItem(itemName:String):Item
		{
			var numDontHaveItems:int = m_dontHaveItems.length;
			
			for(var indItem:int = 0; indItem < numDontHaveItems; indItem++)
			{
				var item:Item = m_dontHaveItems[indItem] as Item;
				
				if(item.name == itemName)
				{
					return item;
				}
			}
			
			return null;
		}
		
		public function getDontHaveItemPrice(itemName:String):uint
		{
			var numDontHaveItems:int = m_dontHaveItems.length;
			
			for(var indItem:int = 0; indItem < numDontHaveItems; indItem++)
			{
				var item:Item = m_dontHaveItems[indItem] as Item;
				
				if(item.name == itemName)
				{
					return item.price;
				}
			}
			
			return 0;
		}
		
		private function checkIsItemInHaveItems(itemName:String):Boolean
		{
			for(var indHaveItem:int = 0; indHaveItem < m_haveItems.length; indHaveItem++)
			{
				if(m_haveItems[indHaveItem] == itemName)
				{
					return true;
				}
			}
			
			return false;
		}
		 
		
		private function saveHaveItem(itemName:String):void
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			
			var haveItemsNames:Array = null;
			
			if(sharedObject.data.haveItems)
			{
				haveItemsNames = sharedObject.data.haveItems as Array;
			}
			else
			{
				haveItemsNames = new Array();
			}
			
			haveItemsNames.push(itemName);
			
			sharedObject.data.haveItems = haveItemsNames;
			sharedObject.flush();
		}
		
		private function saveDontHaveItem(itemName:String):void
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			
			var dontHaveItemsNames:Array = sharedObject.data.dontHaveItems as Array;
			dontHaveItemsNames.push(itemName);
			
			sharedObject.data.dontHaveItems = dontHaveItemsNames;
			sharedObject.flush();
		}
		
		public function get itemsSprites():Dictionary
		{
			return m_itemsSprites;
		}
		
		public function initSpriteContainerForItem(item:Item):Sprite
		{
			if(!item)
			{
				return null;
			}
			
			var itemContainer:Sprite = null;
			
			if(item.subItems && item.subItems.length > 0)//if its superitem
			{
				itemContainer = new Sprite();
				
				var numCurrentSubItems:int = 0;
				var subItems:Array = item.subItems;
				
				for(var indSubItem:int = 0; indSubItem < subItems.length; indSubItem++)
				{
					var subItemContainer:Sprite = null;
					
					if(subItems[indSubItem] is Item)
					{
						numCurrentSubItems++;
						
						var subItem:Item = subItems[indSubItem];
						subItemContainer = initBitmapItemWithName(subItem.name, false);
					}
					else
					{
						subItemContainer = initBitmapItemWithName(subItems[indSubItem], true);
					}
					
					subItemContainer.scaleX = subItemContainer.scaleY = 0.5;
					putSubItemSpriteInRightPosition(subItemContainer, indSubItem);
					itemContainer.addChild(subItemContainer);
				}
				
				var superItemContainer:Sprite = null;
				
				if(numCurrentSubItems >= subItems.length)
				{
					superItemContainer = initBitmapItemWithName(item.name, false, true, true);
				}
				else
				{
					superItemContainer = initBitmapItemWithName(item.name, true, true);
				}
				
				//superItemContainer.scaleX = superItemContainer.scaleY = 0.8;
				itemContainer.addChild(superItemContainer);
			}
			else if(!item.superItem)//if its common item
			{
				itemContainer = initBitmapItemWithName(item.name, false, false, true);
			}
			
			return itemContainer;
		}
		
		private function putSubItemSpriteInRightPosition(subItemSprite:Sprite, index:int):void
		{
			switch(index)
			{
				case 0:
					subItemSprite.x = -20;
					subItemSprite.y = -10;
					break;
				case 1:
					subItemSprite.x = 150;
					subItemSprite.y = -10;
					break;
				case 2:
					subItemSprite.x = -20;
					subItemSprite.y = 150;
					break;
			}
		}
		
		public function initBitmapItemWithName(itemName:String, isBlack:Boolean, isSuperItem:Boolean = false, isMainItem:Boolean = false):Sprite
		{
			var itemContainer:Sprite = new Sprite();
			itemContainer.name = itemName;
			
			var itemBitmap:Bitmap = bitmapForItemName(itemName);
			itemBitmap.x = 254 / 2 - itemBitmap.width / 2;
			itemBitmap.y = 85;
			
			if(isBlack)
			{
				itemBitmap.transform.colorTransform = new ColorTransform(0, 0, 0, 1);
				
				if(!isSuperItem)
				{
					//add it to check collision in main update loop
					GameStage.instance.addBlackBitmap(itemContainer);
				}
			}
			itemContainer.addChild(itemBitmap);
			
			if(!isBlack)
			{
				var shadow:DropShadowFilter = new DropShadowFilter();
				shadow.distance = 10;
				shadow.angle = 25;
				itemBitmap.filters = [shadow];
				
				var textFormat:TextFormat = new TextFormat("RememberFont", 30);
				textFormat.align = TextFormatAlign.CENTER;
				
				var itemTitle:TextField = new TextField();
				itemTitle.selectable = false;
				itemTitle.defaultTextFormat = textFormat;
				itemTitle.width = 254;
				itemTitle.embedFonts = true;
				itemTitle.height = 30;
				itemTitle.text = m_itemsNamesToShow[itemName].nameShow;
				itemTitle.y = 30;
				//itemTitle.border = true;
				itemContainer.addChild(itemTitle);
				
				//if(isMainItem)
				//{
					var glow:GlowFilter = new GlowFilter();
					glow.color = 0xffffff;
					//glow.alpha = 0.5;
					glow.strength = 10;
					//glow.blurX = 10;
//					glow.blurY = 10;
					itemTitle.filters = [glow];
				//}
			}
			
			return itemContainer;
		}
				
		public function getHaveItem(itemName:String):Item
		{
			for(var indHaveItem:int = 0; indHaveItem < m_haveItems.length; indHaveItem++)
			{
				if((m_haveItems[indHaveItem] as Item).name == itemName)
				{
					return (m_haveItems[indHaveItem] as Item);
				}
			}
			
			return null;
		}
		
		public function getDontHaveItemsNames():Array
		{
			var numDontHaveItems:int = m_dontHaveItems.length;
			
			if(numDontHaveItems == 0)
			{
				return null;
			}
			
			var dontHaveItemsNames:Array = new Array(numDontHaveItems);
			
			for(var indDontHaveItem:int = 0; indDontHaveItem < numDontHaveItems; indDontHaveItem++)
			{
				dontHaveItemsNames[indDontHaveItem] = (m_dontHaveItems[indDontHaveItem] as Item).name;
			}
			
			return dontHaveItemsNames;
		}
		
		public function getHaveItemsNames():Array
		{
			var numHaveItems:int = m_haveItems.length;
			
			if(numHaveItems == 0)
			{
				return null;
			}
			
			var haveItemsNames:Array = new Array(numHaveItems);
			
			for(var indHaveItem:int = 0; indHaveItem < numHaveItems; indHaveItem++)
			{
				haveItemsNames[indHaveItem] = (m_haveItems[indHaveItem] as Item).name;
			}
			
			return haveItemsNames;
		}
	}
}
