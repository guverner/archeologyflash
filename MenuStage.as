﻿package 
{
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.text.TextField;
	import flash.events.Event;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.display.InteractiveObject;
	import flash.media.Sound;
	
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class MenuStage extends CStage 
	{
		private var m_titleLabel:TextField;
		private var m_playButton:CButton;
		private var m_aboutLabel:TextField;
		
		public function MenuStage() 
		{
			super(Constants.MENU);
			
			if (this.stage)
			{
				initialise();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		private function initialise():void
		{
			createBackground();
		
			var resources:Resources = new Resources();
			
			var titleBitmap:Bitmap = new Resources.GameTitleImage() as Bitmap;
			titleBitmap.x = 25;
			titleBitmap.y = 20;
			this.addChild(titleBitmap);
			
			var shellBitmap:Bitmap = new Resources.ShellMenuImage() as Bitmap;
			shellBitmap.x = 20;
			shellBitmap.y = 230;
			this.addChild(shellBitmap);
			
			var elephantBitmap:Bitmap = new Resources.ElephantMenuImage() as Bitmap;
			elephantBitmap.x = 345;
			elephantBitmap.y = 210;
			this.addChild(elephantBitmap);
			
			var textFormat:TextFormat = new TextFormat("RememberFont", 60);
			textFormat.align = TextFormatAlign.CENTER;
			//textFormat.color = 0xEABE5F;
			
			//m_titleLabel = new TextField();
//			m_titleLabel.selectable = false;
//			m_titleLabel.defaultTextFormat = textFormat;
//			m_titleLabel.width = this.stage.stageWidth;
//			m_titleLabel.embedFonts = true;
//			m_titleLabel.height = 60;
//			m_titleLabel.text = "Idle Archeology";
//			m_titleLabel.y = 10;
//			//m_titleLabel.border = true;
//			this.addChild(m_titleLabel);

			m_playButton = new CButton(Resources.ButtonBaseImage, onPlayButtonClick, "Play", 20,
									   1.3, 1.15, 1, Resources.ButtonSound);
			//m_playButton.x = this.stage.stageWidth / 2 - m_playButton.width / 2;
			m_playButton.x = this.stage.stageWidth / 2;
			m_playButton.y = 210;
			this.addChild(m_playButton);
			
			textFormat = new TextFormat("RememberFont", 18);
			textFormat.align = TextFormatAlign.CENTER;
			//textFormat.color = 0xEABE5F;
			
			m_aboutLabel = new TextField();
			m_aboutLabel.selectable = false;
			m_aboutLabel.width = this.stage.stageWidth;
			m_aboutLabel.height = 30;
			m_aboutLabel.defaultTextFormat = textFormat;
			m_aboutLabel.embedFonts = true;
			m_aboutLabel.text = "Made by Vitaliy Vikulov";
			m_aboutLabel.y = this.stage.stageHeight - 40;
			//m_aboutLabel.border = true;
			this.addChild(m_aboutLabel);
		}
		
		private function createBackground():void
		{
			var bmpData:BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight, false, 0x582C2B);//0x4C4C4C
			var bitmap:Bitmap = new Bitmap(bmpData);
			this.addChild(bitmap);
		}
		
		private function onPlayButtonClick(clickPosX:Number, clickPosY:Number):void
		{
			//var buttonSound:Sound = new Resources.ButtonSound() as Sound;
//			buttonSound.play();
			
			//trace("onPlayButtonClick");
			StageManager.instance.popStage();
			StageManager.instance.pushStage(GameStage.instance);
		}
	}
}