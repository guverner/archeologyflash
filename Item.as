﻿package 
{
	import flash.utils.Dictionary;

	public class Item
	{
		private var m_name:String;
		private var m_nameToShow:String;
		private var m_price:uint;
		private var m_subItems:Array;
		private var m_superItem:String;
		
		public function Item(name_:String)
		{
			m_name = name_;
			m_price = findPriceItem(m_name);
			m_superItem = findSuperItem(m_name);
			m_nameToShow = findNameToShow(m_name);
			
			if(!m_superItem)
			{
				m_subItems = findSubItems(m_name);
			}
		}
		
		private function findNameToShow(name_:String):String
		{
			var itemsNamesToShow:Dictionary = ItemsCollection.itemsNamesToShow;
			
			return itemsNamesToShow[name_].nameShow;
		}
		
		private function findSubItems(itemName:String):Array
		{
			var subItemsCopy:Array = new Array;
			
			var superItemsInfo:Dictionary = ItemsCollection.superItemsInfo;
			
			if(superItemsInfo[itemName] != null)
			{
				var subItems:Array = superItemsInfo[itemName];
				
				for(var indSubItem:int = 0; indSubItem < subItems.length; indSubItem++)
				{
					subItemsCopy.push(subItems[indSubItem]);
				}
			}
			
			if(subItemsCopy.length > 0)
			{
				return subItemsCopy;
			}
			else
			{
				return null;
			}
		}
		
		private function findPriceItem(itemName:String):uint
		{
			var subItemsInfo:Dictionary = ItemsCollection.subItemsInfo;
			
			if(subItemsInfo[itemName] != null)
			{
				return subItemsInfo[itemName].price;
			}
			
			return 0;
		}
		
		private function findSuperItem(itemName:String):String
		{
			var subItemsInfo:Dictionary = ItemsCollection.subItemsInfo;
			
			if(subItemsInfo[itemName] != null)
			{
				return subItemsInfo[itemName].superitem;
			}
			
			return null;
		}
		
		public function get nameToShow():String
		{
			return m_nameToShow;
		}
		
		public function get name():String
		{
			return m_name;
		}
		
		public function get superItem():String
		{
			return m_superItem;
		}
		
		public function get price():uint
		{
			return m_price;
		}
		
		public function get subItems():Array
		{
			return m_subItems;
		}
	}
}
