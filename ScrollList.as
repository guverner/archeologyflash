﻿package
{
	import flash.display.Sprite;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextField;
	import flash.display.Bitmap;
	import flash.geom.ColorTransform;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.display.Shape;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;

	public class ScrollList extends Sprite
	{
		private var m_inputRectangle:Shape;
		private var m_scrollContainer:Sprite;
		private var m_timeLastFrame:int;
		private var m_heightAllCells:Number;
		
		private var m_dragBarContainer:Sprite;
		private var m_dragBar:Bitmap;
		private var m_dragPin:Bitmap;
		
		private var m_isMouseDown:Boolean;
		private var m_isMouseOverScrollContainer:Boolean;
		private var m_prevY:int;
		private var m_curY:int;
		private static const DIRRECTION_UP:int = 	0;
		private static const DIRRECTION_DOWN:int = 	1;
		private static const DIRRECTION_NONE:int = 	2;
		
		public function ScrollList()
		{
			super();
			
			if (this.stage)
			{
				initialise();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		private function initialise():void
		{
			m_timeLastFrame = getTimer();
			m_isMouseOverScrollContainer = false;
			m_isMouseDown = false;
			
			m_inputRectangle = new Shape();
			m_inputRectangle.graphics.beginFill(0xFF0000, 0.5);
				m_inputRectangle.graphics.drawRect(0, 0, 340, 269);
			m_inputRectangle.graphics.endFill();
			this.addChild(m_inputRectangle);
			
			m_scrollContainer = new Sprite();
			m_scrollContainer.mouseEnabled = false;
			m_scrollContainer.mouseChildren = false;
			m_scrollContainer.scrollRect = new Rectangle(0, 0, 360, 269);
			this.addChild(m_scrollContainer);
			
			m_dragBarContainer = new Sprite();
			m_dragBarContainer.y = 5;
			this.addChild(m_dragBarContainer);
			
			m_dragBar = new Resources.DragBarImage() as Bitmap;
			m_dragBarContainer.addChild(m_dragBar);
			
			m_dragBarContainer.x = this.width - m_dragBar.width;
			
			m_dragPin = new Resources.PinImage() as Bitmap;
			m_dragPin.scaleY = 10;
			m_dragPin.scaleX = 0.8;
			m_dragPin.x = 2;
			m_dragPin.y = 1;
			m_dragBarContainer.addChild(m_dragPin);
			
			this.addEventListener(MouseEvent.MOUSE_OVER, onOverScrollList);
			this.addEventListener(MouseEvent.MOUSE_OUT, onOutScrollList);
			this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownEvent);
			this.addEventListener(MouseEvent.MOUSE_UP, onMouseUpEvent);
			this.addEventListener(Event.ENTER_FRAME, onEnterFrameEvent);
		}
		
		public function updatePinSize():void
		{
			m_heightAllCells = m_scrollContainer.height;
			
			//trace("heightAllCells = " + m_heightAllCells);
//			trace("m_inputRectangle = " + m_inputRectangle.height);
			
			var pinSize100Percent:Number = m_inputRectangle.height - 7;
			
			if(m_heightAllCells > m_inputRectangle.height)
			{
				var pinPercentSize:Number = pinSize100Percent * 100 / m_heightAllCells;
				var pinPixelsSize:Number = pinSize100Percent * pinPercentSize / 100;
				
				m_dragPin.scaleY = pinPixelsSize;
			}
			else
			{
				m_dragPin.scaleY = pinSize100Percent;
			}
		}
		
		public function remove():void
		{
			this.removeEventListener(Event.ENTER_FRAME, onEnterFrameEvent);
		}
		
		private function onEnterFrameEvent(e:Event):void
		{
			var time:int = getTimer();
			var deltaTime:Number = (time - m_timeLastFrame) * 0.001;
			m_timeLastFrame = time;
			
			m_prevY = m_curY;
			m_curY = stage.mouseY;
			
			if(m_isMouseDown && m_isMouseOverScrollContainer)
			{
				var direction:int = getDirection(m_curY, m_prevY);
				
				const MOVE_AMMOUNT:Number = 200 * deltaTime;
				//trace("MOVE_AMMOUNT = " + MOVE_AMMOUNT);
				
				var percentPerDrag:Number = MOVE_AMMOUNT * 100 / m_heightAllCells;
				var pixelsDragPin:Number = m_dragBarContainer.height * percentPerDrag / 100 + 0.1;
				
				var newY:Number = 0;
				
				if(direction ==  DIRRECTION_DOWN)
				{
					newY = m_scrollContainer.scrollRect.y - MOVE_AMMOUNT;
					
					if(newY < 0)
					{
						newY = 0;
					}
					else
					{
						//move pin only if can move cells
						m_dragPin.y -= pixelsDragPin;//1.848;
						
						if(m_dragPin.y < 1)
						{
							m_dragPin.y = 1;
						}
					}
					
					m_scrollContainer.scrollRect = new Rectangle(0, newY, 360, 269);
				}
				else if(direction ==  DIRRECTION_UP)
				{
					newY = m_scrollContainer.scrollRect.y + MOVE_AMMOUNT;
					
					//trace("m_scrollContainer.height = " + m_heightAllCells);
//					trace("m_scrollContainer.scrollRect.height = " + m_scrollContainer.scrollRect.height);
//					trace((m_heightAllCells - m_scrollContainer.scrollRect.height));
					
					if(newY > m_heightAllCells - m_scrollContainer.scrollRect.height - 2)
					{
						newY = m_heightAllCells - m_scrollContainer.scrollRect.height - 2;
					}
					else
					{
						//move pin only if can move cells
						m_dragPin.y += pixelsDragPin;//1.848;
						
						trace("1 = " + m_dragPin.y + m_dragPin.height);
						trace("2 = " + m_dragBarContainer.height);
						
						if(m_dragPin.y + m_dragPin.height > 264)
						{
							m_dragPin.y = 264 - m_dragPin.height;
						}
					}
					
					m_scrollContainer.scrollRect = new Rectangle(0, newY, 360, 269);
				}
			}
		}
		
		private function onMouseDownEvent(e:MouseEvent):void
		{
			m_isMouseDown = true;
		}
		
		private function onMouseUpEvent(e:MouseEvent):void
		{
			m_isMouseDown = false;
		}
		
		private function onOutScrollList(e:MouseEvent):void
		{
			m_isMouseOverScrollContainer = false;
			m_isMouseDown = false;
			//trace("out");
		}
		
		private function onOverScrollList(e:MouseEvent):void
		{
			m_isMouseOverScrollContainer = true;
			//trace("over");
		}
		
		private function getDirection(currentPos:Number, prevPos:Number):int
		{
			var direction:int = DIRRECTION_NONE;
			
			if(prevPos > currentPos)
			{
				direction = DIRRECTION_UP;
			}
			else if(prevPos < currentPos)
			{
				direction = DIRRECTION_DOWN;
			}
			
			return direction;
		}
		
		public function addCell(name_:String, Texture:Class):Sprite
		{
			var itemCell:Sprite = new Sprite();
			//this.addChild(itemCell);
			m_scrollContainer.addChild(itemCell);
			
			var textFormat:TextFormat = new TextFormat("RememberFont", 20);
			textFormat.align = TextFormatAlign.CENTER;
			
			var itemName:TextField = new TextField();
			itemName.selectable = false;
			itemName.defaultTextFormat = textFormat;
			itemName.width = 360;
			itemName.embedFonts = true;
			//itemName.border = true;
			itemName.height = 25;
			itemName.text = name_;
			//itemName.x = m_menuContainer.width / 2 - itemName.width / 2;
			itemName.y = 9;
			itemCell.addChild(itemName);
			
			var bitmapItem:Bitmap = new Texture() as Bitmap;
			bitmapItem.transform.colorTransform = new ColorTransform(0, 0, 0, 1);
			//bitmapItem.x = m_menuContainer.width / 2 - bitmapItem.width / 2;
			bitmapItem.x = 360 / 2 - bitmapItem.width / 2;
			bitmapItem.y = 34;
			itemCell.addChild(bitmapItem);
			
			var lineUp:Bitmap = new Resources.LineImage() as Bitmap;
			//lineUp.y = 140;
			itemCell.addChild(lineUp);
			
			var lineDown:Bitmap = new Resources.LineImage() as Bitmap;
			lineDown.y = 147;
			itemCell.addChild(lineDown);
			
			return itemCell;
		}
	}
}
