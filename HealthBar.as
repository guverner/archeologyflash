﻿package
{
	import flash.display.Sprite;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.events.Event;

	public class HealthBar extends Sprite
	{
		private static var 	m_instance:HealthBar = null;
		private static var 	m_isAllowInstantiation:Boolean = false;
		
		private var m_label:TextField;
		
		public function HealthBar()
		{
			super();
			
			if (!m_isAllowInstantiation)
			{
				throw new Error("Error: Instantiation failed: Use HealthBar.instance instead of new.");
			}
			else
			{
				if (this.stage)
				{
					initialise();
				}
				else
				{
					this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
				}
			}
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		public static function get instance():HealthBar
		{
			if (m_instance == null) {
				m_isAllowInstantiation = true;
				m_instance = new HealthBar();
				m_isAllowInstantiation = false;
			}
			
			return m_instance;
		}
		
		private function initialise():void
		{
			var textFormat:TextFormat = new TextFormat("RememberFont", 30);
			textFormat.align = TextFormatAlign.CENTER;
			
			m_label = new TextField();
			m_label.selectable = false;
			m_label.defaultTextFormat = textFormat;
			m_label.width = GameStage.instance.stage.stageWidth;
			m_label.embedFonts = true;
			m_label.height = 30;
			m_label.text = 0 + "/" + 0;
			//m_label.border = true;
			this.addChild(m_label);
		}
		
		public function changeHealth(currentHealth:int, maxHealth:int):void
		{
			m_label.text = currentHealth + "/" + maxHealth;
		}
	}
}
