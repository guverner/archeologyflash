﻿package 
{
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class Utils
	{
		public function Utils()
		{

		}

		public static function randomRangeFloat(minNum:Number, maxNum:Number):Number
		{
			return (Math.random() * (maxNum - minNum + 1) + minNum);
		}

		public static function randomRangeInt(minNum:Number, maxNum:Number):Number
		{
			return (Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum);
		}
	}
}