﻿package
{
	import flash.net.SharedObject;
	import flash.display.Bitmap;
	import flash.display.Sprite;

	public class Cell extends CButton
	{
		private var m_row:int;
		private var m_column:int;
		private static var UNIQ_CELL_CHANCE:int = 15;
		private var m_health:int;
		private var m_healthMax:int;
		public static const TYPE_GRASS:int = 1;
		public static const TYPE_EARTH:int = 2;
		public static const TYPE_CHEST:int = 3;
		private var m_type:int;
		private var m_currentCrashImageName:String;
		private var m_crashImagesContainer:Sprite;
		private var m_callback:Function;
		
		public function Cell(texture:Class, callback:Function, row:int, column:int)
		{
			//super(texture, callback, null, 16, 1, 0.95, 0.8);
			//super(texture, callbackCell, null, 16, 0.25, 0.22, 0.18);
			super(texture, callbackCell, null, 16, 1, 0.97, 0.73);
			
			m_callback = callback;
			
			m_row = row;
			m_column = column;
			
			if(texture == Resources.TileGrassImage)
			{
				m_type = TYPE_GRASS;
			}
			else if(texture == Resources.TileEarthImage)
			{
				m_type = TYPE_EARTH;
			}
			else if(texture == Resources.TileChestImage)
			{
				m_type = TYPE_CHEST;
			}
			
			m_crashImagesContainer = new Sprite();
			this.addChild(m_crashImagesContainer);
			
			changeCurrentCrashImageName();
			initCrashImages();
			
			m_crashImagesContainer.x -= m_crashImagesContainer.width / 2;
			m_crashImagesContainer.y -= m_crashImagesContainer.height / 2;
			
			var crashImage:Bitmap = getCurrentCrashBitmap();
			crashImage.visible = true;
			
			if(m_currentCrashImageName == "TileEarthImage")
			{
				crashImage.alpha = 1;
			}
			else
			{
				crashImage.alpha = 0;
			}
			
			this.callbackOnOver = onCallbackOverCell;
			this.callbackOnOut = onCallbackOutCell;
		}

		private function getCurrentCrashBitmap():Bitmap
		{
			var crashImage:Bitmap = m_crashImagesContainer.getChildByName(m_currentCrashImageName) as Bitmap;
			return crashImage;
		}

		private function callbackCell(clickPosX:Number, clickPosY:Number):void
		{
			m_callback(clickPosX, clickPosY, this);
		}

		public function onCallbackOverCell():void
		{
			var gameLayer:Sprite = GameStage.instance.getLayer(GameStage.LAYER_GAME) as Sprite;
			gameLayer.swapChildren(this, gameLayer.getChildAt(gameLayer.numChildren - 1));
			
			HealthBar.instance.changeHealth(m_health, m_healthMax);
			HealthBar.instance.visible = true;
		}
		
		public function onCallbackOutCell():void
		{
			HealthBar.instance.visible = false;
		}
		
		public function get crashImagesContainer():Sprite
		{
			return m_crashImagesContainer;
		}
		
		private function initCrashImages():void
		{
			var crashEarthBitmap:Bitmap = new Resources.CrashEarthImage() as Bitmap;
			crashEarthBitmap.name = "CrashEarthImage";
			crashEarthBitmap.alpha = 0;
			crashEarthBitmap.visible = false;
			m_crashImagesContainer.addChild(crashEarthBitmap);
			
			var tileEarthBitmap:Bitmap = new Resources.TileEarthImage() as Bitmap;
			tileEarthBitmap.name = "TileEarthImage";
			tileEarthBitmap.alpha = 0;
			tileEarthBitmap.visible = false;
			m_crashImagesContainer.addChild(tileEarthBitmap);
			
			var crashBitmap:Bitmap = new Resources.CrashImage() as Bitmap;
			crashBitmap.name = "CrashImage";
			crashBitmap.alpha = 1;
			crashBitmap.visible = false;
			m_crashImagesContainer.addChild(crashBitmap);
		}
		
		public function get type():int
		{
			return m_type;
		}
		
		public function get health():int
		{
			return m_health;
		}
		
		public function get healthMax():int
		{
			return m_healthMax;
		}
		
		public function changeHealth(newHealth:int):void
		{
			m_health = newHealth;
			m_healthMax = newHealth;
			
			HealthBar.instance.changeHealth(m_health, m_healthMax);
			
			saveCurrentAndMaxHealth();
		}
		
		private function saveCurrentAndMaxHealth():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			sharedObject.data.cellHealth = m_health;
			sharedObject.data.cellHealthMax = m_healthMax;
			sharedObject.flush();
		}
		
		//when current health and max Health should be different
		//change current health and max health
		public function changeHealthDifferent(currentHealth:int, maxHealth:int):void
		{
			m_health = currentHealth;
			m_healthMax = maxHealth;
			
			HealthBar.instance.changeHealth(m_health, m_healthMax);
			
			saveCurrentAndMaxHealth();
			
			changeCrashImageAlpha();
		}
		
		public function receiveDamage(damage:uint):Boolean
		{
			m_health -= damage;
			
			if(m_health <= 0)
			{
				m_health = 0;
				
				destroyed();
				
				changeCell();
				
				return true;
			}
			else
			{
				changeCrashImageAlpha();
			
				var sharedObject:SharedObject = SharedObject.getLocal("game");
				
				var cellInfo:Object = (sharedObject.data.cells as Array)[m_row][m_column];
				cellInfo.health = m_health;
				
				(sharedObject.data.cells as Array)[m_row][m_column] = cellInfo;
				sharedObject.flush();
				
				HealthBar.instance.changeHealth(m_health, m_healthMax);
				
				return false;
			}
		}
		
		private function changeCrashImageAlpha():void
		{
			//var changeAmmount:Number = 1 / (m_healthMax - 1);
			var currentAlpha:Number = (m_health - 1) / m_healthMax;
			var currentCrashImage:Bitmap = getCurrentCrashBitmap();
			
			if(m_currentCrashImageName == "TileEarthImage")
			{
				//currentCrashImage.alpha -= changeAmmount;
				currentCrashImage.alpha = currentAlpha;
			}
			else
			{
				//currentCrashImage.alpha += changeAmmount;
				currentCrashImage.alpha = 1 - currentAlpha;
			}
			
			if(currentCrashImage.alpha > 1)
			{
				currentCrashImage.alpha = 1;
			}
			else if(currentCrashImage.alpha < 0)
			{
				currentCrashImage.alpha = 0;
			}
		}
		
		private function destroyed():void
		{
			GameStage.instance.cellDestroyed(this);
		}
		
		private function changeCurrentCrashImageName():void
		{
			switch(m_type)
			{
				case TYPE_CHEST:
					m_currentCrashImageName = "TileEarthImage";
					break;
				case TYPE_EARTH:
					m_currentCrashImageName = "CrashImage";
					break;
				case TYPE_GRASS:
					m_currentCrashImageName = "CrashEarthImage";
					break;
			}
		}
		
		private function changeCrashImage():void
		{
			var pastCrashImage:Bitmap = m_crashImagesContainer.getChildByName(m_currentCrashImageName) as Bitmap;
			pastCrashImage.alpha = 0;
			pastCrashImage.visible = false;
			
			changeCurrentCrashImageName();
			
			var crashImage:Bitmap = m_crashImagesContainer.getChildByName(m_currentCrashImageName) as Bitmap;
			
			switch(m_currentCrashImageName)
			{
				case "CrashImage":
					crashImage.alpha = 0;
					break;
				case "CrashEarthImage":
					crashImage.alpha = 0;
					break;
				case "TileEarthImage":
					crashImage.alpha = 1;
					break;
			}
			
			crashImage.visible = true;
		}
		
		private function changeCell():void
		{
			var textureToUse:Class = null;
			
			if(GameStage.instance.numCellsDestroyed > 0)
			{
				var dontHaveItems:Array = ItemsCollection.instance.dontHaveItems;
				
				if(dontHaveItems.length > 0 && Utils.randomRangeInt(0, 100) <= UNIQ_CELL_CHANCE)
				{
					textureToUse = Resources.TileChestImage;
					m_type = TYPE_CHEST;
				}
				else
				{
					textureToUse = Resources.TileEarthImage;
					m_type = TYPE_EARTH;
				}
			}
			else
			{
				textureToUse = Resources.TileGrassImage;
				m_type = TYPE_GRASS;
			}
			
			changeCrashImage();
			changeTexture(textureToUse);
			
			var numHealth:int = 0;
			
			if(textureToUse == Resources.TileChestImage)
			{
				numHealth = Utils.randomRangeInt(25, 50) + int(GameStage.instance.numCellsDestroyed / 5) * 2;
			}
			else
			{
				numHealth = Utils.randomRangeInt(5, 10) + int(GameStage.instance.numCellsDestroyed / 5);
			}
			
			this.changeHealth(numHealth);
			
			//changeCrashImageAlpha();
			
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			
			var cellInfo:Object = new Object();
			cellInfo.health = m_health;
			cellInfo.healthMax = m_healthMax;
			cellInfo.type = m_type;
			
			(sharedObject.data.cells as Array)[m_row][m_column] = cellInfo;
			
			//sharedObject.data.cellType = m_type;
//			sharedObject.data.cellHealth = m_health;
//			sharedObject.data.cellHealthMax = m_healthMax;
			sharedObject.flush();
		}
	}
}
