﻿package
{
	import flash.events.Event;
	import flash.display.Sprite;
	import flash.display.Bitmap;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.geom.Point;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.ColorTransform;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	import flash.geom.Rectangle;
	import flash.display.Shape;
	import fl.transitions.Tween;
	import fl.transitions.easing.None;
	import fl.transitions.easing.Bounce;
	import fl.transitions.easing.Strong;
	import fl.transitions.TweenEvent;
	import flash.utils.Dictionary;

	public class ItemsMenuStage extends CStage 
	{
		private var m_menuContainer:Sprite;
		private var m_menuBase:Bitmap;
		private var m_closeButton:CButton;
		private var m_leftButton:CButton;
		private var m_rightButton:CButton;
		private var m_titleMenu:TextField;
		private var m_countItemsLabel:TextField;
		//private var m_scrollList:ScrollList;
		private var m_isMouseDown:Boolean;
		private var m_timeLastFrame:int;
		private var m_currentItemIndex:int;
		private var m_itemsContainer:Sprite;
		
		public function ItemsMenuStage()
		{
			super(Constants.ITEMS);
			
			if (this.stage)
			{
				initialise();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		private function initialise():void
		{
			m_timeLastFrame = getTimer();
			m_currentItemIndex = 0;
			
			m_menuContainer = new Sprite();
//			m_menuContainer.y = stage.stageHeight / 2 - 100;
			this.addChild(m_menuContainer);
			
			m_menuBase = new Resources.ItemsMenuBaseImage() as Bitmap;
			m_menuContainer.addChild(m_menuBase);
			
			m_menuContainer.x = stage.stageWidth / 2 - m_menuContainer.width / 2;
			
			m_closeButton = new CButton(Resources.CloseButtonImage, onCloseButtonClick, null, 16,
										1.3, 1.15, 1, Resources.ButtonSound);
			m_closeButton.x = m_menuContainer.width - m_closeButton.width / 2 - 10;
			m_closeButton.y = m_closeButton.height / 2 + 5;
			m_menuContainer.addChild(m_closeButton);
			
			var textFormat:TextFormat = new TextFormat("RememberFont", 50);
			textFormat.align = TextFormatAlign.CENTER;
			
			var itemsLabelBitmap:Bitmap = new Resources.ItemsLabelImage() as Bitmap;
			itemsLabelBitmap.x = m_menuContainer.width / 2 - itemsLabelBitmap.width / 2;
			m_menuContainer.addChild(itemsLabelBitmap);
			
			m_titleMenu = new TextField();
			m_titleMenu.selectable = false;
			m_titleMenu.defaultTextFormat = textFormat;
			m_titleMenu.width = 250;
			m_titleMenu.embedFonts = true;
			m_titleMenu.height = 40;
			m_titleMenu.text = "Items";
			m_titleMenu.x = m_menuContainer.width / 2 - m_titleMenu.width / 2;
			//m_titleMenu.y = 5;
			//m_titleMenu.border = true;
			//m_menuContainer.addChild(m_titleMenu);
			
			textFormat = new TextFormat("RememberFont", 30);
			textFormat.align = TextFormatAlign.CENTER;
			
			m_countItemsLabel = new TextField();
			m_countItemsLabel.selectable = false;
			m_countItemsLabel.defaultTextFormat = textFormat;
			m_countItemsLabel.width = 100;
			m_countItemsLabel.embedFonts = true;
			m_countItemsLabel.height = 40;
			m_countItemsLabel.text = ItemsCollection.instance.haveItems.length + "/" +
									(ItemsCollection.instance.haveItems.length +
									 ItemsCollection.instance.dontHaveItems.length);
			m_countItemsLabel.x = 5;// m_menuContainer.width / 2 - m_countItemsLabel.width / 2;
			m_countItemsLabel.y = 8;
			//m_countItemsLabel.border = true;
			m_menuContainer.addChild(m_countItemsLabel);
			
			m_itemsContainer = new Sprite();
			m_itemsContainer.x = 52;
			m_itemsContainer.y = 55;
			m_itemsContainer.scrollRect = new Rectangle(0, 0, 254, 250);
			m_menuContainer.addChild(m_itemsContainer);
			
			var rectangle:Shape = new Shape();
			rectangle.graphics.beginFill(0x000000, 0.2);
				rectangle.graphics.drawRect(0, 0, 254, 250);
			rectangle.graphics.endFill();
			m_itemsContainer.addChild(rectangle);
			
			var haveItems:Array = ItemsCollection.instance.haveItems;
			var itemShow:Item = haveItems[m_currentItemIndex];
			var itemsSprites:Dictionary = ItemsCollection.instance.itemsSprites;
			
			if(haveItems.length > 0)
			{
				trace("You have items");
				trace("Num items = " + haveItems.length);
				
				m_rightButton = new CButton(Resources.ArrowButtonImage, onRightArrowButtonClick, null, 16,
											1.3, 1.15, 1, Resources.ButtonSound);
				m_rightButton.x = 333;
				m_rightButton.y = 190;
				m_menuContainer.addChild(m_rightButton);
				
				m_leftButton = new CButton(Resources.ArrowButtonImage, onLeftArrowButtonClick, null, 16,
										   1.3, 1.15, 1, Resources.ButtonSound);
				m_leftButton.rotation = 180;
				m_leftButton.x = 26;
				m_leftButton.y = 190;
				m_leftButton.visible = false;
				m_menuContainer.addChild(m_leftButton);
				
				var itemContainer:Sprite = itemsSprites[itemShow.name];
				itemContainer.x = 0;
				itemContainer.y = 0;
				//m_menuContainer.addChild(itemContainer);
				m_itemsContainer.addChild(itemContainer);

				//m_itemContainer = ItemsCollection.instance.itemsSprites[m_currentItemIndex];
//				m_itemContainer.x = 0;
//				m_itemContainer.y = 56;
//				m_menuContainer.addChild(m_itemContainer);
				
				if(haveItems.length == 1)
				{
					//m_rightButton.disable();
					//m_leftButton.disable();
					m_rightButton.visible = false;
					m_leftButton.visible = false;
					
					//var crossButtonDissable:Bitmap = new Resources.CrossImage() as Bitmap;
//					crossButtonDissable.x = -crossButtonDissable.width / 2;
//					crossButtonDissable.y = -crossButtonDissable.height / 2;
//					m_rightButton.addChild(crossButtonDissable);
//					crossButtonDissable = new Resources.CrossImage() as Bitmap;
//					crossButtonDissable.x = -crossButtonDissable.width / 2;
//					crossButtonDissable.y = -crossButtonDissable.height / 2;
//					m_leftButton.addChild(crossButtonDissable);
				}
			}
			else
			{
				trace("You DONT have an items");
				
				textFormat = new TextFormat("RememberFont", 40);
				textFormat.align = TextFormatAlign.CENTER;
				
				var infoLabel:TextField = new TextField();
				infoLabel.selectable = false;
				infoLabel.wordWrap = true;
				infoLabel.defaultTextFormat = textFormat;
				infoLabel.width = 250;
				infoLabel.embedFonts = true;
				infoLabel.height = 90;
				infoLabel.text = "You don't have items";
				infoLabel.x = m_menuContainer.width / 2 - infoLabel.width / 2;
				infoLabel.y = 135;
				//infoLabel.border = true;
				m_menuContainer.addChild(infoLabel);
			}
			
			//m_scrollList = new ScrollList();
//			m_scrollList.y = 51;
//			m_menuContainer.addChild(m_scrollList);
			//
//			var itemCell1:Sprite = m_scrollList.addCell("Ancient Jug", Resources.VazaImage);
//			var itemCell2:Sprite = m_scrollList.addCell("Skull", Resources.SkullImage);
//			itemCell2.y += itemCell1.height - 7;
//			var itemCell3:Sprite = m_scrollList.addCell("Skull", Resources.SkullImage);
//			itemCell3.y += itemCell1.height * 2 - 7 * 2;
//			
//			m_scrollList.updatePinSize();
		}
		
//		private function onMouseDownEvent(e:MouseEvent):void
//		{
//			m_isMouseDown = true;
//		}
//		
//		private function onMouseUpEvent(e:MouseEvent):void
//		{
//			m_isMouseDown = false;
//		}
		
		private function onRightArrowButtonClick(clickPosX:Number, clickPosY:Number):void
		{
			trace("onRightArrowButtonClick");
			
			m_rightButton.disable();
			m_leftButton.disable();
			
			m_currentItemIndex++;
			
			trace("m_currentItemIndex = " + m_currentItemIndex);
			
			hideNecessaryButtons();
			
			var itemContainerToMove:Sprite = m_itemsContainer.getChildAt(1) as Sprite;
			
			var tweenMoveLeft:Tween = new Tween(itemContainerToMove, "x", Strong.easeIn, itemContainerToMove.x, -220, 0.8, true);
			tweenMoveLeft.addEventListener(TweenEvent.MOTION_FINISH, onItemMoveOutFinish);
			tweenMoveLeft.start();
		}
		
		private function hideNecessaryButtons():void
		{
			var numHaveItems:int = ItemsCollection.instance.haveItems.length;
			
			if(m_currentItemIndex >= numHaveItems - 1)
			{
				m_rightButton.visible = false;
				m_leftButton.visible = true;
			}
			else if(m_currentItemIndex <= 0)
			{
				m_rightButton.visible = true;
				m_leftButton.visible = false;
			}
			else
			{
				m_rightButton.visible = true;
				m_leftButton.visible = true;
			}
		}
		
		public function get itemsContainer():Sprite
		{
			return m_itemsContainer;
		}
		
		public function addItemContainer(itemContainer:Sprite):void
		{
			m_itemsContainer.addChild(itemContainer);
		}
		
		private function onItemMoveOutFinish(e:TweenEvent):void
		{
			//trace("onItemMoveLeftFinish");
			
			m_itemsContainer.removeChildAt(1);
			
			var itemSprites:Dictionary = ItemsCollection.instance.itemsSprites;
			var itemShow:Item = ItemsCollection.instance.haveItems[m_currentItemIndex];
			
			var itemContainer:Sprite = itemSprites[itemShow.name];
			itemContainer.x = 0;
			itemContainer.y = 0;
			//m_menuContainer.addChild(itemContainer);
			trace("m_itemsContainer numChilds = " + m_itemsContainer.numChildren);
			m_itemsContainer.addChild(itemContainer);
			
			m_rightButton.enable();
			m_leftButton.enable();
		}
		
		private function onLeftArrowButtonClick(clickPosX:Number, clickPosY:Number):void
		{
			trace("onLeftArrowButtonClick");
			
			m_rightButton.disable();
			m_leftButton.disable();
			
			m_currentItemIndex--;
			
			trace("m_currentItemIndex = " + m_currentItemIndex);
			
			hideNecessaryButtons();
			
			var itemContainerToMove:Sprite = m_itemsContainer.getChildAt(1) as Sprite;
			
			var tweenMoveRight:Tween = new Tween(itemContainerToMove, "x", Strong.easeIn, itemContainerToMove.x, 220, 0.8, true);
			tweenMoveRight.addEventListener(TweenEvent.MOTION_FINISH, onItemMoveOutFinish);
			tweenMoveRight.start();
		}
		
		private function onCloseButtonClick(clickPosX:Number, clickPosY:Number):void
		{
			//m_scrollList.remove();
			StageManager.instance.popStage();
			GameStage.instance.continueGame();
		}
	}
}
