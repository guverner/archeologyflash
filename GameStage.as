﻿package 
{
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	import flash.display.BitmapData;
	import flash.ui.Keyboard;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.display.Sprite;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextField;
	import flash.net.SharedObject;
	import flash.display.Shape;
	import fl.transitions.Tween;
	import fl.transitions.easing.Elastic;
	import fl.transitions.easing.None;
	import fl.transitions.TweenEvent;
	import flash.utils.getTimer;
	import flash.filters.GlowFilter;
	import flash.filters.BitmapFilterQuality;
	import flash.geom.Point;
	import com.dougmccune.HitTester;
	import flash.events.MouseEvent;
	import com.greensock.TweenLite;
	import com.greensock.easing.Bounce;
	import com.greensock.*;
	import flash.filters.*;
	import com.greensock.plugins.*;
	import com.greensock.TweenMax;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class GameStage extends CStage 
	{	
		private static var m_instance:GameStage;
		private static var m_allowInstantiation:Boolean;
		
		public static const LAYER_BACKGROUND:String = 	"background";
		public static const LAYER_GAME:String = 		"game";
		public static const LAYER_HUD:String = 			"hud";
		
		private var m_pauseButton:CButton;
		private var m_itemsButton:CButton;
		
		private var 		m_layers:Object;
		private var 		m_timers:Array;
		
		private var 		m_timeLastFrame:int;
		private var 		m_isPaused:Boolean;
		public static var 	m_timeTotal:Number;
		
		private var 		m_numClicksTotal:uint;
		private var 		m_numCellsDestroyed:uint;
		private var 		m_damagePerClick:uint;
		private static const START_CELL_HEALTH:int = 5;
		private static const DAMAGE_PER_CLICK_START:int = 1;
		private var 		m_newItemContainer:Sprite;
		private var 		m_popupContainer:Sprite;
		private var 		m_glowFilter:GlowFilter;
		private var 		m_blackBitmapsForCollision:Array;
		private var 		m_isItemsMenuOpen:Boolean;
		private var 		m_blackItemPopup:Sprite;
		private var 		m_mouseClickPoint:Point;
		private var 		m_particlesFromCell:Array;
		
		private var 		m_coinsLabelContainer:Sprite;
		private var 		m_coinsLabel:TextField;
		
		private var 		m_coins:int;
		private static const COINS_PER_CHEST:int = 10;
		private static const COINS_PER_TILE:int = 1;
		
		//all about pickaxe
		private const 		START_PICKAXE_PRICE:uint = 10;
		private var 		m_pickaxeUpdatePrice:uint;
		private var 		m_pickaxeContainer:Sprite;
		private var 		m_pickaxeDamageLabel:TextField;
		private var 		m_pickaxeUpdateButton:CButton;
		
		//all about passive income
		private const 		START_PASSIVE_INCOME_COINS:uint = 0;
		private var 		m_passiveIncomeCoins:uint;//per second
		private var 		m_gnomeUpdateButton:CButton;
		private const 		START_GNOME_UPDATE_PRICE:uint = 20;
		private var 		m_gnomeUpdatePrice:uint;
		private var 		m_gnomeContainer:Sprite;
		private var 		m_timerPassiveIncome:Timer;
		private static const TIME_SEC_UPDATE_PASSIVE_INCOME:Number = 1;
		
		//all about cells
		private const NUM_CELLS_ROWS:int = 5;
		private const NUM_CELLS_COLUMNS:int = 5;
		private var m_cells:Array;

		public function GameStage() 
		{
			super(Constants.GAME);
			
			if (!m_allowInstantiation)
			{
				throw new Error("Error: Instantiation failed: Use GameStage.instance instead of new.");
			}
			else
			{
				if (this.stage)
				{
					initialise();
				}
				else
				{
					this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
				}
			}
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		public static function get instance():GameStage
		{
			if (!m_instance) {
				m_allowInstantiation = true;
				m_instance = new GameStage();
				m_allowInstantiation = false;
			}
			
			return m_instance;
		}
		
		public function remove():void
		{
			removeAllLayers();
			m_instance = null;
		}
		
		private function initialise():void
		{
			m_particlesFromCell = new Array();
			
			m_layers = initLayers();
			m_timers = new Array();
			m_blackBitmapsForCollision = new Array();
			m_isItemsMenuOpen = false;
			m_isPaused = false;
			
			createBackground();
			
			var stonesBcgrBitmap:Bitmap = new Resources.StonesBackgroundImage() as Bitmap;
			getLayer(LAYER_BACKGROUND).addChild(stonesBcgrBitmap);
			
			m_glowFilter = new GlowFilter();
			m_glowFilter.color = 0x377200;
			m_glowFilter.alpha = 1;
			m_glowFilter.blurX = m_glowFilter.blurY = 20;
			m_glowFilter.quality = BitmapFilterQuality.MEDIUM;
			
			HealthBar.instance.y = 285;
			this.getLayer(LAYER_GAME).addChild(HealthBar.instance);
			HealthBar.instance.visible = false;
			
			clearSaves();
			loadResults();
			saveResults();
			
			createInterface();
			
			this.addEventListener(Event.ENTER_FRAME, update);
			this.stage.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDownEvent);
			this.stage.addEventListener(MouseEvent.MOUSE_UP, onMouseUpEvent);
		}
		
		private function onPassiveIncomeTick(e:TimerEvent):void
		{
			if(m_passiveIncomeCoins <= 0)
			{
				return;
			}
			
			this.setCoins(m_passiveIncomeCoins);
			
			var coinBitmap:Bitmap = new Resources.CoinImage() as Bitmap;
			coinBitmap.x = 410;
			coinBitmap.y = 140;
			this.getLayer(LAYER_GAME).addChild(coinBitmap);
			
			TweenMax.to(coinBitmap, 0.5, {y:4, x:335, alpha:0, onComplete:removeBitmapFromGameLayer, onCompleteParams:[coinBitmap]});
		}
		
		private function initCoinsContainer():Sprite
		{
			var coinsLabelContainer:Sprite = new Sprite();
			
			var coinBitmap:Bitmap = new Resources.CoinImage() as Bitmap;
			//coinBitmap.scaleX = coinBitmap.scaleY = 0.7;
			coinsLabelContainer.addChild(coinBitmap);
			
			m_coinsLabel = this.initLabel(m_coins.toString(), 20, 100, 25, TextFormatAlign.LEFT);
			m_coinsLabel.x = 40;
			m_coinsLabel.y = 6;
			//m_coinsLabel.border = true;
			coinsLabelContainer.addChild(m_coinsLabel);
			
			return coinsLabelContainer;
		}
		
		private function createInterface():void
		{
			m_pauseButton = new CButton(Resources.PauseButtonImage, onPauseButtonClick, null, 16, 1.3, 1.15, 1,
										Resources.ButtonSound);
			m_pauseButton.x = m_pauseButton.width / 2 + 8;
			m_pauseButton.y = m_pauseButton.height / 2 + 8;
			getLayer(LAYER_HUD).addChild(m_pauseButton);
			
			m_itemsButton = new CButton(Resources.ButtonBaseSmallImage, onItemsButtonClick, "Items", 16, 1.3, 1.15, 1,
										Resources.ButtonSound);
			m_itemsButton.label.y -= 4;
			m_itemsButton.x = this.stage.stageWidth / 2;
			m_itemsButton.y = m_itemsButton.height / 2 + 8;
			getLayer(LAYER_HUD).addChild(m_itemsButton);
			
			m_pickaxeContainer = initPickaxe();
			m_pickaxeContainer.x = 10;
			m_pickaxeContainer.y = 125;
			getLayer(LAYER_HUD).addChild(m_pickaxeContainer);
			
			m_coinsLabelContainer = initCoinsContainer();
			m_coinsLabelContainer.x = 335; 
			m_coinsLabelContainer.y = 4; 
			getLayer(LAYER_HUD).addChild(m_coinsLabelContainer);
			
			m_gnomeContainer = initGnomeDigger();
			m_gnomeContainer.x = 377;
			m_gnomeContainer.y = 100;
			getLayer(LAYER_HUD).addChild(m_gnomeContainer);
		}
		
		private function initPickaxe():Sprite
		{
			var pickaxeContainer = new Sprite();
			
			var pickaxeBitmap:Bitmap = new Resources.PickaxeImage() as Bitmap;
			pickaxeContainer.addChild(pickaxeBitmap);
			
			m_pickaxeDamageLabel = this.initLabel("Damage:\n" + m_damagePerClick, 20, 100, 50);
			m_pickaxeDamageLabel.x = -20;
			m_pickaxeDamageLabel.y = 60;
			//m_pickaxeDamageLabel.border = true;
			m_pickaxeDamageLabel.multiline = true;
			pickaxeContainer.addChild(m_pickaxeDamageLabel);
			
			var pickaxeNameLabel:TextField = this.initLabel("Pickaxe", 20, 63, 25);
			//pickaxeNameLabel.border = true;
			pickaxeNameLabel.y -= 23;
			pickaxeContainer.addChild(pickaxeNameLabel);
			
			m_pickaxeUpdateButton = new CButton(Resources.PickaxeUpdateButtonImage, onPickaxeUpdateButtonClick, null, 16,
												1.3, 1.15, 1, Resources.ButtonSound);
			m_pickaxeUpdateButton.callbackOnOver = onPickaxeUpdateButtonOver;
			m_pickaxeUpdateButton.callbackOnOut = onPickaxeUpdateButtonOut;
			m_pickaxeUpdateButton.y = 30;
			m_pickaxeUpdateButton.x = 80;
			pickaxeContainer.addChild(m_pickaxeUpdateButton);
			
			updateGlowForThing("pickaxe");
			
			//if(m_pickaxeUpdatePrice <= m_coins)
//			{
//				m_pickaxeUpdateButton.filters = [m_glowFilter];
//			}
			
			return pickaxeContainer;
		}
		
		private function initGnomeDigger():Sprite
		{
			var gnomeContainer:Sprite = new Sprite();
			
			var bitmapGnomeDigger:Bitmap = new Resources.GnomeDiggerImage() as Bitmap;
			gnomeContainer.addChild(bitmapGnomeDigger);
			
			var textFormat:TextFormat = new TextFormat("RememberFont", 19);
			textFormat.align = TextFormatAlign.CENTER;
			
			var nameLabel:TextField = new TextField();
			nameLabel.y -= 25;
			nameLabel.x -= 10;
			nameLabel.selectable = false;
			nameLabel.defaultTextFormat = textFormat;
			//nameLabel.border = true;
			nameLabel.width = 120;
			nameLabel.embedFonts = true;
			nameLabel.height = 25;
			nameLabel.text = "Gnome Digger";
			gnomeContainer.addChild(nameLabel);
			
			var coinsLabel:TextField = new TextField();
			coinsLabel.y += bitmapGnomeDigger.height + 5;
			coinsLabel.x -= 10;
			coinsLabel.selectable = false;
			coinsLabel.defaultTextFormat = textFormat;
			//coinsLabel.border = true;
			coinsLabel.width = 120;
			coinsLabel.embedFonts = true;
			coinsLabel.height = 25;
			coinsLabel.text = "Coins:";
			gnomeContainer.addChild(coinsLabel);
			
			var speedLabel:TextField = new TextField();
			speedLabel.y += bitmapGnomeDigger.height + 25;
			speedLabel.x -= 10;
			speedLabel.selectable = false;
			speedLabel.defaultTextFormat = textFormat;
			//speedLabel.border = true;
			speedLabel.width = 120;
			speedLabel.embedFonts = true;
			speedLabel.height = 25;
			speedLabel.text = m_passiveIncomeCoins + "/sec";
			speedLabel.name = "speed";
			gnomeContainer.addChild(speedLabel);
			
			m_gnomeUpdateButton = new CButton(Resources.PickaxeUpdateButtonImage, onGnomeUpdateButtonClick, null, 16,
											  1.3, 1.15, 1, Resources.ButtonSound);
			m_gnomeUpdateButton.callbackOnOver = onGnomeUpdateButtonOver;
			m_gnomeUpdateButton.callbackOnOut = onGnomeUpdateButtonOut;
			m_gnomeUpdateButton.y = 100;
			m_gnomeUpdateButton.x = 5;
			gnomeContainer.addChild(m_gnomeUpdateButton);
			
			updateGlowForThing("gnome");
			
			return gnomeContainer;
		}
		
		private function onMouseDownEvent(e:MouseEvent):void
		{
			m_mouseClickPoint = new Point(e.stageX, e.stageY);
		}
		
		private function onMouseUpEvent(e:MouseEvent):void
		{
			m_mouseClickPoint = null;
		}
		
		public function addBlackBitmap(bitmapContainer:Sprite):Boolean
		{
			//check it there is no the same black bitmap
			var numBlackBitmaps:int = m_blackBitmapsForCollision.length;
			for(var indBitmap:int = 0; indBitmap < numBlackBitmaps; indBitmap++)
			{
				//if((m_blackBitmapsForCollision[indBitmap] as Sprite).name == bitmapContainer.name)
//				{
//					return false;
//				}

				if((m_blackBitmapsForCollision[indBitmap] as Sprite) == bitmapContainer)
				{
					return false;
				}
			}
			
			m_blackBitmapsForCollision.push(bitmapContainer);
			
			return true;
		}
		
		public function removeBlackBitmap(itemName:String):Boolean
		{
			var numBlackBitmaps:int = m_blackBitmapsForCollision.length;
			for(var indBitmap:int = 0; indBitmap < numBlackBitmaps; indBitmap++)
			{
				if((m_blackBitmapsForCollision[indBitmap] as Sprite).name == itemName)
				{
					m_blackBitmapsForCollision.splice(indBitmap, 1);
					return true;
				}
			}
			
			return false;
		}
		
		private function onGnomeUpdateButtonOut():void
		{
			if(!m_popupContainer)
			{
				return;
			}
			
			m_popupContainer.visible = false;
		}
		
		private function onGnomeUpdateButtonOver():void
		{
			if(!m_popupContainer)
			{
				m_popupContainer = initPopupWithPrice(m_gnomeUpdatePrice);
				this.getLayer(LAYER_HUD).addChild(m_popupContainer);
			}
			
			m_popupContainer.visible = true;
			var priceLabel:TextField = m_popupContainer.getChildByName("priceLabel") as TextField;
			priceLabel.text = "Price: " + m_gnomeUpdatePrice;
			
			m_popupContainer.x = 300;
			m_popupContainer.y = 90;
			
			updatePriceLabelColorForThing("gnome");
		}
		
		private function onPickaxeUpdateButtonOut():void
		{
			if(!m_popupContainer)
			{
				return;
			}
			
			m_popupContainer.visible = false;
		}
		
		private function onPickaxeUpdateButtonOver():void
		{
			if(!m_popupContainer)
			{
				m_popupContainer = initPopupWithPrice(m_pickaxeUpdatePrice);
				this.getLayer(LAYER_HUD).addChild(m_popupContainer);
			}
			
			m_popupContainer.visible = true;
			var priceLabel:TextField = m_popupContainer.getChildByName("priceLabel") as TextField;
			priceLabel.text = "Price: " + m_pickaxeUpdatePrice;
			
			m_popupContainer.x = 10;
			m_popupContainer.y = 50;
			
			updatePriceLabelColorForThing("pickaxe");
		}
		
		private function updatePriceLabelColorForThing(thingType:String):void
		{
			if(!m_popupContainer)
			{
				return;
			}
			
			var priceLabel:TextField = m_popupContainer.getChildByName("priceLabel") as TextField;
			
			var price:uint = 0;
			
			switch(thingType)
			{
				case "pickaxe":
					price = m_pickaxeUpdatePrice;
					break;
				case "gnome":
					price = m_gnomeUpdatePrice;
					break;
			}
			
			if(price <= m_coins)
			{
				priceLabel.textColor = 0x377200;
			}
			else
			{
				priceLabel.textColor = 0x8b0000;
			}
		}
		
		private function initPopupWithPrice(updatePrice:int):Sprite
		{
			var popupContainer:Sprite = new Sprite();
			
			var popupBitmap:Bitmap = new Resources.PickaxeInfoPopupImage() as Bitmap;
			popupContainer.addChild(popupBitmap);
			
			var priceLabel:TextField = this.initLabel("Price: " + updatePrice, 20, popupBitmap.width - 20, 20);
			priceLabel.name = "priceLabel";
			priceLabel.x = 10;
			priceLabel.y = 25;
			///pickaxePriceLabel.border = true;
			popupContainer.addChild(priceLabel);
			
			return popupContainer;
		}
		
		private function onGnomeUpdateButtonClick(clickPosX:Number, clickPosY:Number):void
		{
			if(m_gnomeUpdatePrice <= m_coins)
			{
				setCoins(-m_gnomeUpdatePrice);
				
				m_passiveIncomeCoins++;
				
				m_gnomeUpdatePrice += 15;
				
				var speedLabel:TextField = m_gnomeContainer.getChildByName("speed") as TextField;
				speedLabel.text = m_passiveIncomeCoins + "/sec";
				
				var sharedObject:SharedObject = SharedObject.getLocal("game");
				sharedObject.data.passiveIncomeCoins = m_passiveIncomeCoins;
				sharedObject.data.gnomeUpdatePrice = m_gnomeUpdatePrice;
				sharedObject.flush();
				
				var priceLabel:TextField = m_popupContainer.getChildByName("priceLabel") as TextField;
				priceLabel.text = "Price: " + m_gnomeUpdatePrice;
				
				if(!m_timerPassiveIncome)
				{
					m_timerPassiveIncome = new Timer(TIME_SEC_UPDATE_PASSIVE_INCOME * 1000, 0);
					m_timerPassiveIncome.addEventListener(TimerEvent.TIMER, onPassiveIncomeTick);
					m_timerPassiveIncome.start();
				}
			}
		}
		
		private function onPickaxeUpdateButtonClick(clickPosX:Number, clickPosY:Number):void
		{
			trace("onPickaxeUpdateButtonClick");
			
			if(m_pickaxeUpdatePrice <= m_coins)
			{
				trace("update pickaxe");
				
				setCoins(-m_pickaxeUpdatePrice);
				
				m_pickaxeUpdatePrice = m_damagePerClick * 10;
				
				m_damagePerClick++;
				m_pickaxeDamageLabel.text = "Damage:\n" + m_damagePerClick;
				
				var sharedObject:SharedObject = SharedObject.getLocal("game");
				sharedObject.data.damagePerClick = m_damagePerClick;
				sharedObject.data.pickaxeUpdatePrice = m_pickaxeUpdatePrice;
				sharedObject.flush();
				
				var priceLabel:TextField = m_popupContainer.getChildByName("priceLabel") as TextField;
				priceLabel.text = "Price: " + m_pickaxeUpdatePrice;
			}
		}
		
		private function updateCoinsLabel():void
		{
			m_coinsLabel.text = m_coins.toString();
			
			TweenLite.to(m_coinsLabelContainer, 0.2, {scaleX:1.5, scaleY:1.5});
			TweenLite.to(m_coinsLabelContainer, 0.2, {delay:0.2,  scaleX:1, scaleY:1});
			
			//m_coinsLabel.scaleX = m_coinsLabel.scaleY = 4;
		}
		
		private function updateGlowForThing(thingType:String):void
		{
			var updatePrice:uint = 0;
			var updateButton:CButton = null;
			
			switch(thingType)
			{
				case "pickaxe":
					updatePrice = m_pickaxeUpdatePrice;
					updateButton = m_pickaxeUpdateButton;
					break;
				case "gnome":
					updatePrice = m_gnomeUpdatePrice;
					updateButton = m_gnomeUpdateButton;
					break;
			}
			
			if(updatePrice <= m_coins)
			{
				if(updateButton.filters.length == 0)
				{
					updateButton.filters = [m_glowFilter];
				}
			}
			else
			{
				updateButton.filters = null;
			}
		}
		
		private function initLabel(text_:String, size_:Number, width_:Number, height_:Number, align_:String = "center"):TextField
		{
			var textFormat:TextFormat = new TextFormat("RememberFont", size_);
			textFormat.align = align_;
			
			var coinsLabel:TextField = new TextField();
			coinsLabel.selectable = false;
			coinsLabel.defaultTextFormat = textFormat;
			coinsLabel.width = width_;
			coinsLabel.embedFonts = true;
			coinsLabel.height = height_;
			coinsLabel.text = text_;
			
			return coinsLabel;
		}
		
		private function onItemsButtonClick(clickPosX:Number, clickPosY:Number):void
		{
			//trace("onItemsButtonClick");
			
			m_isPaused = true;
			m_isItemsMenuOpen = true;
			StageManager.instance.pushStage(new ItemsMenuStage());
		}
		
		public function clearSaves():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			sharedObject.clear();
		}
		
		public function saveResults():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			sharedObject.data.numClicksTotal = m_numClicksTotal;
			sharedObject.data.numCellsDestroyed = m_numCellsDestroyed;
			sharedObject.data.damagePerClick = m_damagePerClick;
			sharedObject.data.haveItems = ItemsCollection.instance.getHaveItemsNames();
			sharedObject.data.coins = m_coins;
			sharedObject.data.passiveIncomeCoins = m_passiveIncomeCoins;
			
			var cellsTemp:Array = new Array(NUM_CELLS_ROWS);
			
			var indRow:int;
			var indColumn:int;
			
			for(indRow = 0; indRow < NUM_CELLS_ROWS; indRow++)
			{
				cellsTemp[indRow] = new Array(NUM_CELLS_COLUMNS);
			}
			
			for(indRow = 0; indRow < NUM_CELLS_ROWS; indRow++)
			{
				for(indColumn = 0; indColumn < NUM_CELLS_COLUMNS; indColumn++)
				{
					var cell:Cell = m_cells[indRow][indColumn] as Cell;
					
					var cellInfo:Object = new Object();
					cellInfo.health = cell.health;
					cellInfo.healthMax = cell.healthMax;
					cellInfo.type = cell.type;
					cellsTemp[indRow][indColumn] = cellInfo;
				}
			}
			
			sharedObject.data.cells = cellsTemp;
			
			sharedObject.flush();
		}
		
		public function loadResults():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			
			if(sharedObject.data.numClicksTotal)
			{
				m_numClicksTotal = sharedObject.data.numClicksTotal;
			}
			else
			{
				m_numClicksTotal = 0;
			}
			
			if(sharedObject.data.numCellsDestroyed)
			{
				m_numCellsDestroyed = sharedObject.data.numCellsDestroyed;
			}
			else
			{
				m_numCellsDestroyed = 0;
			}
			
			if(sharedObject.data.damagePerClick)
			{
				m_damagePerClick = sharedObject.data.damagePerClick;
			}
			else
			{
				m_damagePerClick = DAMAGE_PER_CLICK_START;
			}
			
			m_cells = new Array(NUM_CELLS_ROWS);
				
			var indRow:int;
			
			for(indRow = 0; indRow < NUM_CELLS_ROWS; indRow++)
			{
				m_cells[indRow] = new Array(NUM_CELLS_COLUMNS);
			}
			
			const extraMoveX:Number = 135;
			const extraMoveY:Number = 60;
			var indColumn:int;
			var cell:Cell = null;
			
			if(sharedObject.data.cells)
			{
				var loadedCellsInfo:Array = sharedObject.data.cells;
				
				for(indRow = 0; indRow < NUM_CELLS_ROWS; indRow++)
				{
					for(indColumn = 0; indColumn < NUM_CELLS_COLUMNS; indColumn++)
					{
						var cellInfo:Object = loadedCellsInfo[indRow][indColumn];
						var cellHealth:int = cellInfo.health;
						var cellHealthMax:int = cellInfo.healthMax;
						var cellType:int = cellInfo.type;
						
						var texture:Class = null;
						
						switch(cellType)
						{
							case Cell.TYPE_GRASS:
								texture = Resources.TileGrassImage;
								break;
							case Cell.TYPE_EARTH:
								texture = Resources.TileEarthImage;
								break;
							case Cell.TYPE_CHEST:
								texture = Resources.TileChestImage;
								break;
						}
						
						cell = new Cell(texture, onCellClick, indRow, indColumn);
						cell.changeHealthDifferent(cellHealth, cellHealthMax);
						cell.x = cell.width/ 2 + (cell.width - 2) * indColumn + extraMoveX;
						cell.y = cell.height/ 2 + (cell.height - 2) * indRow + extraMoveY;
						this.getLayer(LAYER_GAME).addChild(cell);
						m_cells[indRow][indColumn] = cell;
					}
				}
			}
			else
			{
				for(indRow = 0; indRow < NUM_CELLS_ROWS; indRow++)
				{
					for(indColumn = 0; indColumn < NUM_CELLS_COLUMNS; indColumn++)
					{
						cell = new Cell(Resources.TileGrassImage, onCellClick, indRow, indColumn);
						cell.changeHealth(START_CELL_HEALTH);
						cell.x = cell.width/ 2 + (cell.width - 2) * indColumn + extraMoveX;
						cell.y = cell.height/ 2 + (cell.height - 2) * indRow + extraMoveY;
						this.getLayer(LAYER_GAME).addChild(cell);
						m_cells[indRow][indColumn] = cell;
					}
				}
			}
			
			if(sharedObject.data.coins)
			{
				m_coins = sharedObject.data.coins;
			}
			else
			{
				m_coins = 0;
			}
			
			if(sharedObject.data.pickaxeUpdatePrice)
			{
				m_pickaxeUpdatePrice = sharedObject.data.pickaxeUpdatePrice;
			}
			else
			{
				m_pickaxeUpdatePrice = START_PICKAXE_PRICE;
			}
			
			if(sharedObject.data.gnomeUpdatePrice)
			{
				m_gnomeUpdatePrice = sharedObject.data.gnomeUpdatePrice;
			}
			else
			{
				m_gnomeUpdatePrice = START_GNOME_UPDATE_PRICE;
			}
			
			if(sharedObject.data.passiveIncomeCoins)
			{
				m_passiveIncomeCoins = sharedObject.data.passiveIncomeCoins;
			}
			else
			{
				m_passiveIncomeCoins = START_PASSIVE_INCOME_COINS;
			}
			
			if(m_passiveIncomeCoins > 0 && !m_timerPassiveIncome)
			{
				m_timerPassiveIncome = new Timer(TIME_SEC_UPDATE_PASSIVE_INCOME * 1000, 0);
				m_timerPassiveIncome.addEventListener(TimerEvent.TIMER, onPassiveIncomeTick);
				m_timerPassiveIncome.start();
			}
		}
		
		public function get numCellsDestroyed():uint
		{
			return m_numCellsDestroyed;
		}
		
		public function set numCellsDestroyed(newValue:uint):void
		{
			m_numCellsDestroyed = newValue;
		}
		
		public function get coins():int
		{
			return m_coins;
		}
		
		private function setCoins(ammountCoins:int):void
		{
			m_coins += ammountCoins;
			
			if(m_coins < 0)
			{
				m_coins = 0;
			}
			
			saveCoins();
			updateCoinsLabel();
			updateGlowForThing("pickaxe");
			updateGlowForThing("gnome");
			updatePriceLabelColorForThing("pickaxe");
			updatePriceLabelColorForThing("gnome");
			updateBlackItemPopup();
		}
		
		public function cellDestroyed(cell:Cell):void
		{
			TweenMax.to(cell, 0.2, {rotation:360});
			
			m_numCellsDestroyed += 1;
			
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			sharedObject.data.numCellsDestroyed = m_numCellsDestroyed;
			sharedObject.flush();
			
			if(cell.type == Cell.TYPE_CHEST)
			{
				setCoins(COINS_PER_CHEST);
				
				var randomItem:Item = giveRandomItem();
				
				if(!randomItem)//if player 
				{
					return;
				}
				
				ItemsCollection.instance.addHaveItem(randomItem);
				
				this.mouseEnabled = false;
				
				m_newItemContainer = new Sprite();
				this.addChild(m_newItemContainer);
				
				var rectangle:Shape = new Shape();
				rectangle.graphics.beginFill(0x000000, 1);
					rectangle.graphics.drawRect(0, 0, this.stage.stageWidth, this.stage.stageHeight);
				rectangle.graphics.endFill();
				rectangle.alpha = 0;
				m_newItemContainer.addChild(rectangle);
				
				var itemContainer:Sprite = new Sprite();
				itemContainer.x = this.stage.stageWidth / 2;
				itemContainer.y = this.stage.stageHeight / 2;
				m_newItemContainer.addChild(itemContainer);
				
				var itemBitmap:Bitmap = ItemsCollection.instance.bitmapForItemName(randomItem.name);
				itemBitmap.scaleX = itemBitmap.scaleY = 0.5;
				itemBitmap.x -= itemBitmap.width / 2;
				itemBitmap.y -= itemBitmap.height / 2;
				itemContainer.addChild(itemBitmap);
				
				var scaleeFactor:Number = 3;
				var time:Number = 2;
				
				var tweenOpacity:Tween = new Tween(rectangle, "alpha", None.easeOut, rectangle.alpha, 0.7, 0.2, true);
				tweenOpacity.start();
				
				var tweenScaleX:Tween = new Tween(itemContainer, "scaleX", Elastic.easeOut, itemBitmap.scaleX, scaleeFactor, time, true);
				tweenScaleX.start();
				var tweenScaleY:Tween = new Tween(itemContainer, "scaleY", Elastic.easeOut, itemBitmap.scaleX, scaleeFactor, time, true);
				tweenScaleY.start();
				tweenScaleY.addEventListener(TweenEvent.MOTION_FINISH, onNewItemScaleFinish);
			}
			else
			{
				setCoins(COINS_PER_TILE);
			}
		}
		
		private function removeBitmapFromGameLayer(bitmap:Bitmap):void
		{
			this.getLayer(LAYER_GAME).removeChild(bitmap);
		}
		
		private function saveCoins():void
		{
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			sharedObject.data.coins = m_coins;
			sharedObject.flush();
		}
		
		private function onNewItemScaleFinish(e:TweenEvent):void
		{
			var timer:CTimer = new CTimer(1, 0.7, onNewItemWaitTimeFinish);
		}
		
		private function onNewItemWaitTimeFinish():void
		{
			this.mouseEnabled = true;
			this.removeChild(m_newItemContainer);
			m_newItemContainer = null;
		}
		
		private function giveRandomItem():Item
		{
			var dontHaveItems:Array = ItemsCollection.instance.dontHaveItems;
			
			if(dontHaveItems.length == 0)
			{
				return null;
			}
			
			var randItemIndex:int = Utils.randomRangeInt(0, dontHaveItems.length - 1);
			var randomItem:Item = dontHaveItems[randItemIndex];
			
			return randomItem;
		}
		
		private function onCellClick(clickPosX:Number, clickPosY:Number, cell:Cell):void
		{
			m_numClicksTotal++;
			
			throwParticleFromCell(clickPosX, clickPosY, cell.type);//throw grass or earth pieces
			
			var isCellDestroyed:Boolean = cell.receiveDamage(m_damagePerClick);
			
			if(isCellDestroyed)
			{
				var coinBitmap:Bitmap = new Resources.CoinImage() as Bitmap;
				coinBitmap.x = clickPosX;
				coinBitmap.y = clickPosY;
				this.getLayer(LAYER_GAME).addChild(coinBitmap);
				
				TweenMax.to(coinBitmap, 0.5, {y:4, x:335, alpha:0, onComplete:removeBitmapFromGameLayer, onCompleteParams:[coinBitmap]});
			}
			
			var sharedObject:SharedObject = SharedObject.getLocal("game");
			sharedObject.data.numClicksTotal = m_numClicksTotal;
			sharedObject.flush();
		}
		
		private function throwParticleFromCell(clickPosX:Number, clickPosY:Number, cellType:int):void
		{
			var particleBitmap:Bitmap = null;
			
			if(cellType == Cell.TYPE_GRASS)
			{
				particleBitmap = new Resources.GrassParticleImage() as Bitmap;
			}
			else
			{
				particleBitmap = new Resources.EarthParticleImage() as Bitmap;
			}
			
			particleBitmap.x = clickPosX - particleBitmap.width / 2;
			particleBitmap.y = clickPosY - particleBitmap.height / 2;
			getLayer(LAYER_GAME).addChild(particleBitmap);
			
			m_particlesFromCell.push(particleBitmap);
			
			TweenLite.to(particleBitmap, 0.3, {x:Main.randomRange(particleBitmap.x - 40, particleBitmap.x + 40),
						 						y:Main.randomRange(particleBitmap.y - 40, particleBitmap.y - 60),
												rotation:Main.randomRange(-10, 10),
												onComplete:onParticleFromCellFinish});
		}
		
		private function onParticleFromCellFinish():void
		{
			var particle:Bitmap = m_particlesFromCell.shift();
			getLayer(LAYER_GAME).removeChild(particle);
		}
		
		private function createBackground():void
		{
			var bmpData:BitmapData = new BitmapData(stage.stageWidth, stage.stageHeight, false, 0x582C2B);
			var bitmap:Bitmap = new Bitmap(bmpData);
			getLayer(LAYER_BACKGROUND).addChild(bitmap);
		}
		
		public function get timers():Array
		{
			return m_timers;
		}
		
		private function removeAllLayers():void
		{
			if (!m_layers)
			{
				return;
			}
			
			for (var key:String in m_layers)
			{
				this.removeChild((m_layers[key] as Sprite));
			}
			
			m_layers = null;
		}
		
		public function getLayer(layerName:String):Sprite
		{
			if (m_layers.hasOwnProperty(layerName))
			{
				return (m_layers[layerName] as Sprite);
			}
			
			return null;
		}
		
		private function initLayers():Object
		{
			var layers:Object = new Object();
			
			layers[LAYER_BACKGROUND] = new Sprite();
			this.addChild(layers[LAYER_BACKGROUND]);
			layers[LAYER_GAME] = new Sprite();
			this.addChild(layers[LAYER_GAME]);
			layers[LAYER_HUD] = new Sprite();
			this.addChild(layers[LAYER_HUD]);
			
			return layers;
		}
		
		private function onKeyPressed(e:KeyboardEvent):void
		{
		}
		
		private function onKeyReleased(e:KeyboardEvent):void
		{
		}
		
		private function updateBlackItemPopup():void
		{
			if(!m_blackItemPopup)
			{
				return;
			}
			
			var itemCollidedName:String = m_blackItemPopup.name;
			var itemPrice:uint = ItemsCollection.instance.getDontHaveItemPrice(itemCollidedName);
			var priceLabel:TextField = m_blackItemPopup.getChildAt(1) as TextField;
			var clickToBuyLabel:TextField = m_blackItemPopup.getChildAt(2) as TextField;
			
			if(itemPrice <= m_coins)
			{
				priceLabel.textColor = 0x377200;
				clickToBuyLabel.textColor = 0x377200;
				clickToBuyLabel.text = "Click to buy";
			}
			else
			{
				priceLabel.textColor = 0x8b0000;
				clickToBuyLabel.textColor = 0x8b0000;
				clickToBuyLabel.text = "Not enought money";
			}
		}
		
		private function checkMouseBlackItemsCollision():void
		{
			var itemCollidedName:String = isMouseBlackBitmapsCollide();
			
			if(itemCollidedName != null)//if collided with some black item
			{
				if(!m_blackItemPopup)
				{
					m_blackItemPopup = initBlackItemPopup(itemCollidedName,
														  ItemsCollection.instance.getDontHaveItemPrice(itemCollidedName));
					stage.addChild(m_blackItemPopup);
					m_blackItemPopup.visible = false;
				}
				
				if(!m_blackItemPopup.visible){
					var itemPrice:uint = ItemsCollection.instance.getDontHaveItemPrice(itemCollidedName);
					
					var priceLabel:TextField = m_blackItemPopup.getChildAt(1) as TextField;
					priceLabel.text = "Price: " + itemPrice;
					
					m_blackItemPopup.name = itemCollidedName;
					
					updateBlackItemPopup();
					
					m_blackItemPopup.x = stage.mouseX - 20;
					m_blackItemPopup.y = stage.mouseY + 20;
					m_blackItemPopup.visible = true;
				}
			}
			else if(m_blackItemPopup)//if mouse is not collide and popup was creaded
			{
				m_blackItemPopup.visible = false;
			}
		}
		
		private function initBlackItemPopup(itemName:String, price:uint):Sprite
		{
			var container:Sprite = new Sprite();
			
			var popupBitmap:Bitmap = new Resources.PickaxeInfoPopupImage() as Bitmap;
			container.addChild(popupBitmap);
			
			var textFormat:TextFormat = new TextFormat("RememberFont", 20);
			textFormat.align = TextFormatAlign.CENTER;
			
			var priceLabel:TextField = new TextField();
			//priceLabel.border = true;
			priceLabel.selectable = false;
			priceLabel.defaultTextFormat = textFormat;
			priceLabel.width = popupBitmap.width;
			priceLabel.embedFonts = true;
			priceLabel.height = 20;
			priceLabel.text = "Price: " + price;
			priceLabel.y = 25;
			container.addChild(priceLabel);
			
			if(price <= m_coins)
			{
				priceLabel.textColor = 0x377200;//green
			}
			else
			{
				priceLabel.textColor = 0x8b0000;//red
			}
			
			textFormat = new TextFormat("RememberFont", 15);
			textFormat.align = TextFormatAlign.CENTER;
			
			var youDontHaveEnoughtMoneyLabel:TextField = new TextField();
			//youDontHaveEnoughtMoneyLabel.border = true;
			youDontHaveEnoughtMoneyLabel.selectable = false;
			youDontHaveEnoughtMoneyLabel.defaultTextFormat = textFormat;
			youDontHaveEnoughtMoneyLabel.width = popupBitmap.width;
			youDontHaveEnoughtMoneyLabel.embedFonts = true;
			youDontHaveEnoughtMoneyLabel.height = 20;
			youDontHaveEnoughtMoneyLabel.y = 45;
			container.addChild(youDontHaveEnoughtMoneyLabel);
			
			if(price <= m_coins)
			{
				youDontHaveEnoughtMoneyLabel.textColor = 0x377200;
				youDontHaveEnoughtMoneyLabel.text = "Click to buy";
			}
			else
			{
				youDontHaveEnoughtMoneyLabel.textColor = 0x8b0000;
				youDontHaveEnoughtMoneyLabel.text = "Not enought money";
			}
			
			return container;
		}
		
		private function update(e:Event):void
		{
			if(m_isItemsMenuOpen)
			{
				checkMouseBlackItemsCollision();
			}
			
			if (m_isPaused)
			{
				return;
			}
			
			var time:int = getTimer();
			var deltaTime:Number = (time - m_timeLastFrame) * 0.001;
			m_timeLastFrame = time;
			
			m_timeTotal += deltaTime;
			
			updateTimers(deltaTime);
		}
		
		private function buyClosedItem(itemName:String):Boolean
		{
			var item:Item = ItemsCollection.instance.getDontHaveItem(itemName);
			
			if(!item.superItem)
			{
				return false;
			}
			
			if(item.price <= m_coins)
			{
				setCoins(-item.price);
				
				//remove al balck bitmaps of collision for superitem of this item
				removeAllBlackBitmapsForSuperitem(item.superItem);
				
				ItemsCollection.instance.addHaveItem(item);
				
				//update visual container
				var itemsSprites:Dictionary = ItemsCollection.instance.itemsSprites;
				var itemMenuStage:ItemsMenuStage = StageManager.instance.currentStage() as ItemsMenuStage;
				var itemContainer:Sprite = itemMenuStage.itemsContainer;
				itemContainer.removeChildAt(1);//remove all bitmaps of this item
				itemMenuStage.addItemContainer(itemsSprites[item.superItem]);
				
				return true;
			}
			
			return false;
		}
		
		private function removeAllBlackBitmapsForSuperitem(superItemName:String):void
		{
			var superItemsInfo:Dictionary = ItemsCollection.superItemsInfo;
			
			if(!superItemsInfo || !superItemsInfo[superItemName])
			{
				return;
			}
			
			var subItems:Array = superItemsInfo[superItemName] as Array;
			var numSubItems:int = subItems.length;
			
			for(var indSubItem:int = 0; indSubItem < numSubItems; indSubItem++)
			{
				removeBlackBitmap(subItems[indSubItem]);
			}
		}
		
		private function isMouseBlackBitmapsCollide():String
		{
			var numBlackBitmaps:int = m_blackBitmapsForCollision.length;
			
			for(var indBitmap:int = 0; indBitmap < numBlackBitmaps; indBitmap++)
			{
				var bitmap:Bitmap = (m_blackBitmapsForCollision[indBitmap] as Sprite).getChildAt(0) as Bitmap;
				var mousePoint:Point = new Point(this.stage.mouseX, this.stage.mouseY);
				
				if(HitTester.realHitTest(bitmap, mousePoint))
				{
					if(m_mouseClickPoint && HitTester.realHitTest(bitmap, m_mouseClickPoint) &&
					   buyClosedItem((m_blackBitmapsForCollision[indBitmap] as Sprite).name))
					{
						return null;//item is bought
					}
					
					return (m_blackBitmapsForCollision[indBitmap] as Sprite).name;
				}
			}
			
			return null;
		}
		
		private function updateTimers(dt:Number):void
		{
			var numTimers:uint = m_timers.length;
			for (var indTimer:int = 0; indTimer < numTimers; indTimer++)
			{
				(m_timers[indTimer] as CTimer).update(dt);
			}
		}
		
		private function processKeys(deltaTime:Number):void
		{
		}
		
		private function onPauseButtonClick(clickPosX:Number, clickPosY:Number):void
		{
			//trace("onPauseButtonClick");
			m_isPaused = true;
			StageManager.instance.pushStage(new PauseStage());
		}
		
		public function continueGame():void
		{
			//trace("continueGame");
			m_isPaused = false;
			m_isItemsMenuOpen = false;
		}
	}
}