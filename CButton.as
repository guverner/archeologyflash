﻿package
{
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.text.TextField;
	import flash.text.TextFormatAlign;
	import flash.text.TextFormat;
	import flash.events.MouseEvent;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.media.Sound;

	public class CButton extends Sprite
	{
		private var m_callback:Function;
		private var m_buttonContainer:Sprite;
		private var m_texture:Bitmap;
		private var m_label:TextField;
		private var m_isMouseOver:Boolean;
		private var m_isDisabled:Boolean;
		private var SCALE_NORMAL:Number = 1;
		private var SCALE_OVER:Number = 1.3;
		private var SCALE_DOWN:Number = 1.15;
		private var m_callbackOnOver:Function;
		private var m_callbackOnOut:Function;
		private var m_soundButtonDown:Sound;
		
		public function CButton(texture:Class, callback:Function, text:String = null, textSize:int = 16,
								scaleOver:Number = 1.3, scaleDown:Number = 1.15, scaleNormal:Number = 1, soundButtonDown:Class = null)
		{
			super();
			
			if(soundButtonDown)
			{
				m_soundButtonDown = new soundButtonDown() as Sound;
			}
			
			SCALE_OVER = scaleOver;
			SCALE_DOWN = scaleDown;
			SCALE_NORMAL = scaleNormal;
			
			m_buttonContainer = new Sprite();
			this.addChild(m_buttonContainer);
			
			m_callback = callback;
			m_isMouseOver = false;
			this.buttonMode = true;
			m_isDisabled = false;
			
			m_texture = new texture() as Bitmap;
			m_buttonContainer.addChild(m_texture);
			
			if(text)
			{
				var textFormat:TextFormat = new TextFormat("RememberFont", textSize);
				textFormat.align = TextFormatAlign.CENTER;
				
				m_label = new TextField();
				//m_label.border = true;
				m_label.y = 8;
				m_label.selectable = false;
				m_label.mouseEnabled = false;
				m_label.defaultTextFormat = textFormat;
				m_label.embedFonts = true;
				m_label.text = text;
				m_label.width = this.width;
				m_label.height = this.height - 10;
				m_buttonContainer.addChild(m_label);
			}
			
			m_buttonContainer.x -= m_buttonContainer.width / 2;
			m_buttonContainer.y -= m_buttonContainer.height / 2;
			
			this.addEventListener(MouseEvent.MOUSE_UP, onButtonUp);
			this.addEventListener(MouseEvent.MOUSE_DOWN, onButtonDown);
			this.addEventListener(MouseEvent.MOUSE_OVER, onButtonOver);
			this.addEventListener(MouseEvent.MOUSE_OUT, onButtonOut);
			
			if (this.stage)
			{
				initialise();
			}
			else
			{
				this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			}
			
			this.scaleX = this.scaleY = SCALE_NORMAL;
		}
		
		public function get label():TextField
		{
			return m_label;
		}
		
		private function onButtonUp(e:MouseEvent):void
		{
			if(m_isDisabled)
			{
				return;
			}
			
			//trace("onButtonUp");
			
			if(m_isMouseOver)
			{
				this.scaleX = this.scaleY = SCALE_OVER;
				//trace("callback");
				
				m_callback(e.stageX, e.stageY);
			}
			else
			{
				this.scaleX = this.scaleY = SCALE_NORMAL;
			}
		}
		
		private function onButtonDown(e:MouseEvent):void
		{
			if(m_isDisabled)
			{
				return;
			}
			
			if(m_soundButtonDown)
			{
				m_soundButtonDown.play();
			}
			
			//trace("onButtonDown");
			this.scaleX = this.scaleY = SCALE_DOWN;
		}
		
		private function onButtonOver(e:MouseEvent):void
		{
			if(m_isDisabled)
			{
				return;
			}
			
			//trace("onButtonOver");
			m_isMouseOver = true;
			this.scaleX = this.scaleY = SCALE_OVER;
			
			if(m_callbackOnOver != null)
			{
				m_callbackOnOver();
			}
		}
		
		public function set callbackOnOver(newValue_:Function):void
		{
			m_callbackOnOver = newValue_;
		}
		
		public function set callbackOnOut(newValue_:Function):void
		{
			m_callbackOnOut = newValue_;
		}
		
		private function onButtonOut(e:MouseEvent):void
		{
			if(m_isDisabled)
			{
				return;
			}
			
			//trace("onButtonOut");
			m_isMouseOver = false;
			this.scaleX = this.scaleY = SCALE_NORMAL;
			
			if(m_callbackOnOut != null)
			{
				m_callbackOnOut();
			}
		}
		
		public function changeTexture(texture:Class):void
		{
			m_buttonContainer.removeChild(m_texture);
			m_texture = null;
			m_texture = new texture() as Bitmap;
			m_buttonContainer.addChild(m_texture);
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			initialise();
		}
		
		public function disable():void
		{
			//trace("disable");
			m_isDisabled = true;
			this.scaleX = this.scaleY = SCALE_NORMAL;
		}
		
		public function enable():void
		{
			//trace("enable");
			m_isDisabled = false;
			
			m_isMouseOver = false;
			
			if(this.visible && this.hitTestPoint(stage.mouseX, stage.mouseY, false))
			{
				this.scaleX = this.scaleY = SCALE_OVER;
				m_isMouseOver = true;
			}
		}
		
		private function initialise():void
		{
			
		}
	}
}
