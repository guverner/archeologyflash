﻿package  
{
	/**
	 * ...
	 * @author Vitaliy Vikulov
	 */
	public class Constants 
	{
		public static const MENU:String = 		"MENU";
		public static const GAME:String = 		"GAME";
		public static const PAUSE:String = 		"PAUSE";
		public static const GAME_OVER:String = 	"GAME_OVER";
		public static const ITEMS:String = 	"ITEMS";
		
		public function Constants() 
		{
		}
	}
}